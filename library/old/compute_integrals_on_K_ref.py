from sympy import *

x, y, z = symbols('x y z')

L1 = 1-x-y-z
L2 = x
L3 = y
L4 = z

print integrate(integrate(integrate(L1*L1, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L2*L1, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L3*L1, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L4*L1, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L1*L2, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L2*L2, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L3*L2, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L4*L2, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L1*L3, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L2*L3, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L3*L3, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L4*L3, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L1*L4, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L2*L4, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L3*L4, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
print integrate(integrate(integrate(L4*L4, (z, 0, 1-x-y)), (y, 0, 1-x)), (x, 0, 1))
