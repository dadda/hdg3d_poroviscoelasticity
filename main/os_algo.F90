
program os_algo
#include <petsc/finclude/petscsys.h>
  use petscsys
  use error_handling
  use geometric_structures
  use physics
  use boundary_markers
  use time_stepping
  use exporters
  use hdg_biot
  use prism_1d
  use prism_3d

  implicit none

  !
  !
  !

  character                                 :: mesh_root*1024, mesh_short*128
  character                                 :: ts_type*16, problem_data*128
  character                                 :: exporter_filebasename*1024
  character                                 :: filename*1024, counter_str*128
  character(len=256), dimension(:), pointer :: mesh_base_names => null()
  PetscInt                                  :: i, j, l, p, n_meshes, n_k, n_dt, dim_ode
  PetscInt                                  :: dstart, dend, dofs_per_proc
  PetscInt, dimension(:), pointer           :: k_vect => null()
  PetscBool                                 :: debug
  PetscReal                                 :: t_start, t_end, biot_energy, Pnew, Qnew
  PetscReal, dimension(:), pointer          :: dt => null(), errors_new => null(), Uhat_loc
  PetscReal, dimension(:,:), pointer        :: errors => null()
  type(mesh)                                :: Th
  type(Biot_data)                           :: data_biot
  type(BiotBM)                              :: bc_markers
  type(TS)                                  :: ts_scheme
  type(Biot_HDG)                            :: biot
  type(ode)                                 :: my_ode
  type(exporter)                            :: viewer
  PetscMPIInt                               :: rank, nprocs, rank_tmp
  PetscErrorCode                            :: ierr

  ! ----------------------------------------------------------------

  call PetscInitialize(PETSC_NULL_CHARACTER, ierr)

  call mpi_comm_rank(PETSC_COMM_WORLD, rank, ierr)
  call mpi_comm_size(PETSC_COMM_WORLD, nprocs, ierr)

  ! Data for Biot problem
  call data_biot%construct()

  ! Parse command line
  n_meshes = 0; n_k = 0
  call parse_cmdline(mesh_root, mesh_short, n_meshes, mesh_base_names, n_k, k_vect, &
       & data_biot, t_start, t_end, n_dt, dt, ts_type, problem_data, dim_ode, &
       & exporter_filebasename, debug)

  ! Time marching algorithm
  call ts_scheme%construct(t_start, t_end, ts_type)

  ! ODE to be solved
  call my_ode%construct(dim_ode, ts_scheme)

  ! Set ODE data
  select case (trim(problem_data))
  case ('prism_1d','prism_3d')
     my_ode%action_F             => action_F_prism_1d_OS
     my_ode%assemble_Jacobian    => assemble_Jacobian_prism_1d
     my_ode%action_source        => action_source_prism_1d_OS
     my_ode%set_initial_solution => set_initial_solution_prism_1d
     my_ode%exact_solution       => exact_solution_prism_1d
     my_ode%compute_energy       => compute_energy_prism_1d
     my_ode%compute_dissipation  => compute_dissipation_prism_1d
  end select

  ! Loop over approximation degrees
  do j = 1, n_k

     if (rank == 0) then
        print '("Running tests for k = ",i2)', k_vect(j)
        print *
     end if

     ! Loop over 3D meshes
     do i = 1, n_meshes

        if (rank == 0) then
           print '("Mesh ",i2)', i
           print *
        end if

        ! Load mesh
        call Th%construct(trim(mesh_root) // '/' // trim(mesh_base_names(i)), &
             & debug, ierr); CHKERRQ(ierr)

        call data_biot%set_bc_flags(Th%faces(:,4))

        ! Mesh boundary markers
        call bc_markers%construct(Th)

        ! Construct exporter
        call viewer%construct(trim(mesh_root) // '/' // trim(mesh_base_names(i)), Th)

        ! Initialize HDG discretization of Biot system
        call biot%construct(data_biot, Th, bc_markers, k_vect(j), ts_scheme)

        allocate( errors(n_dt, biot%Nerr+my_ode%Nerr), errors_new(biot%Nerr+my_ode%Nerr) )
        errors = 0.0d0

        ! Loop over time steps
        do l = 1, n_dt
           ts_scheme%t_current = t_start
           ts_scheme%steps     = 0
           ts_scheme%dt        = dt(l)
           ts_scheme%max_steps = ceiling( (t_end-t_start) / dt(l) )

           if (rank == 0) then
              print '("Time step = ", f15.12,A)', dt(l), new_line(' ')
              print '("Time  Q^{n+1}  P^{n+1}  pi^{n+1}  pi_1  Q_1  E_{\Omega}  E_{\Gamma}")'
           end if

           ! Count the number of digits in ts_scheme%max_steps
           write(counter_str,*) ts_scheme%max_steps
           write(counter_str,*) len(trim(adjustl(counter_str)))

           ! Set the initial conditions
           call biot%set_initial_condition()
           call my_ode%set_initial_condition()

           do
              if (ts_scheme%steps == ts_scheme%max_steps) exit

              ! Set the name of the VTK output file
              if (ts_scheme%steps == (ts_scheme%max_steps-1)) then
                 write(filename, '(A,i'//trim(adjustl(counter_str))//'.'//trim(adjustl(counter_str))&
                      &//')') trim(exporter_filebasename), ts_scheme%steps+1

                 call viewer%initialize_ofile(trim(filename))
              end if

              ! -----------------
              ! Solve Biot system
              ! -----------------

              call data_biot%set_values( (/ int(2,kind=kind(l)) /), &
                   & (/ -prism_1d_Cap/(prism_1d_Cap*prism_1d_Res+dt(l))*my_ode%y(1,1) /), &
                   & (/ -prism_1d_Cap/(prism_1d_Cap*prism_1d_Res+dt(l)) /) )

              call ts_scheme%set_problem(biot)
              call ts_scheme%step()

              ! Read the pressure value at the interface and broadcast it
              call VecGetOwnershipRange(biot%Uhat, dstart, dend, ierr); CHKERRQ(ierr)
              dstart = dstart+1

              call VecGetArrayReadF90(biot%Uhat, Uhat_loc, ierr); CHKERRQ(ierr)
              if ( (biot%ibcflags2dofs_f(2) .ge. dstart) .and. &
                   & (biot%ibcflags2dofs_f(2) .le. dend) ) then
                 Pnew = Uhat_loc(biot%ibcflags2dofs_f(2)-dstart+1)
              end if
              call VecRestoreArrayReadF90(biot%Uhat, Uhat_loc, ierr); CHKERRQ(ierr)
              ! Identify the proc owning Pnew
              dofs_per_proc = (biot%Ndofs+nprocs-1)/nprocs
              do rank_tmp = 0, nprocs-1
                 dstart = rank_tmp * dofs_per_proc + 1
                 dend   = min((rank_tmp+1)*dofs_per_proc, biot%Ndofs)
                 if ( (biot%ibcflags2dofs_f(2) .ge. dstart) .and. &
                      & (biot%ibcflags2dofs_f(2) .le. dend) ) exit
              end do
              ! Broadcast the value of Pnew
              call mpi_bcast(Pnew, 1, MPIU_SCALAR, rank_tmp, PETSC_COMM_WORLD, ierr)

              ! Update pi
              my_ode%y(1,1) = dt(l)/(prism_1d_Cap*prism_1d_Res+dt(l))*Pnew + &
                   & prism_1d_Cap*prism_1d_Res/(prism_1d_Cap*prism_1d_Res+dt(l))*my_ode%y(1,1)

              Qnew = (Pnew-my_ode%y(1,1))/prism_1d_Res

              ! ---------
              ! Solve ODE
              ! ---------

              call ts_scheme%set_problem(my_ode)
              call ts_scheme%step()

              ! Print the current solution to the VTK output file
              if (ts_scheme%steps == (ts_scheme%max_steps-1)) then
                 call biot%save_solution(viewer)
              end if
              call biot%compute_energy(energy=biot_energy)

              call data_biot%set_values( (/ int(2,kind=kind(l)) /), (/ Pnew /) )
              errors_new = 0.0d0

              ! Compute errors
              call biot%compute_errors(errors_new(1:biot%Nerr), int(2,kind=kind(l)))
              ! call my_ode%compute_errors(errors_new(biot%Nerr+1:))
              errors(l,:) = max(errors(l,:), errors_new)

              if (ts_scheme%steps == (ts_scheme%max_steps-1)) then
                 call viewer%finalize_ofile()
              end if

              ts_scheme%t_current = ts_scheme%t_current + ts_scheme%dt
              ts_scheme%steps = ts_scheme%steps + 1

              if (rank == 0) then
                 print '(f16.8, 7es20.12)', ts_scheme%t_current, Qnew, Pnew, &
                      & my_ode%y(1,1), my_ode%y(2,1), my_ode%y(3,1), biot_energy, &
                      & my_ode%compute_energy(ts_scheme%t_current)
              end if

           end do

           if (rank == 0) then
              print *
              if (l == 1) then
                 write(*, fmt='(f15.12)', advance='no') dt(1)
                 do p = 1, biot%Nerr+my_ode%Nerr
                    write(*, fmt='("  &  ", es20.12, "  &  { --- }")', advance='no') errors(1,p)
                 end do
                 write(*, fmt='("\\")')
              else
                 write(*, fmt='(f15.12)', advance='no') dt(l)
                 do p = 1, biot%Nerr+my_ode%Nerr
                    write(*, fmt='("  &  ", es20.12, "  &  ", f7.4)', advance='no') &
                         & errors(l,p), log(errors(l-1,p)/errors(l,p)) / log(dt(l-1)/dt(l))
                 end do
                 write(*, fmt='("\\")')
              end if
           end if
        end do

        deallocate(errors, errors_new)
        call biot%destruct()
        call bc_markers%destruct()
        call viewer%destruct()
        call Th%destruct()
     end do

  end do

  ! ----------------------------------------------------------------
  
  call my_ode%destruct()

  nullify(ts_scheme%problem)
  call ts_scheme%destruct()

  deallocate(mesh_base_names)
  deallocate(k_vect)
  deallocate(dt)

  call data_biot%destruct()

  call PetscFinalize(ierr)

  ! ----------------------------------------------------------------

contains

  subroutine parse_cmdline( mesh_root, mesh_short, n_meshes, mesh_base_names, n_k, k_vect, &
       & data_biot, t_start, t_end, n_dt, dt, ts_type, problem_data, dim_ode, &
       & exporter_filebasename, debug )
    implicit none
    character(len=*), intent(out)                          :: mesh_root, mesh_short, ts_type
    character(len=*), intent(out)                          :: problem_data
    character(len=*), intent(out)                          :: exporter_filebasename
    PetscInt, intent(out)                                  :: n_meshes, n_k, n_dt, dim_ode
    character(len=256), dimension(:), pointer, intent(out) :: mesh_base_names
    PetscInt, dimension(:), pointer, intent(out)           :: k_vect
    class(Biot_data), intent(inout)                        :: data_biot
    PetscReal, intent(out)                                 :: t_start, t_end
    PetscReal, dimension(:), pointer, intent(out)          :: dt
    PetscBool, intent(out)                                 :: debug
    PetscInt                                               :: tmp1, tmp2, i
    PetscBool                                              :: set
    PetscErrorCode                                         :: ierr
    character(len=128)                                     :: err_msg
    character(len=PETSC_MAX_PATH_LEN)                      :: buffer

    ierr    = 0
    err_msg = ''

    ! --------------------------------

    call PetscOptionsGetString(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, &
         & '-mesh_root', mesh_root, set, ierr); CHKERRQ(ierr)
    if (.not. set) then
       ierr = 1
       call check_err1(ierr, '(A)', 'Please provide root directory of mesh files')
    end if

    ! --------------------------------

    call PetscOptionsGetString(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, &
         & '-mesh_short', mesh_short, set, ierr); CHKERRQ(ierr)
    if (.not. set) then
       ierr = 1
       call check_err1(ierr, '(A)', 'Please provide short name of mesh files')
    end if

    ! --------------------------------

    call PetscOptionsGetString(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, &
         & '-mesh_base_names', buffer, set, ierr); CHKERRQ(ierr)
    if (.not. set) then
       ierr = 1
       call check_err1(ierr, '(A)', 'Please provide basenames of mesh files')
    end if

    n_meshes = 1
    tmp1     = 0
    tmp2     = index(buffer, ',')
    do
       if (tmp2 == 0) exit
       tmp1     = tmp1 + tmp2
       tmp2     = index(buffer(tmp2+1:), ',')
       n_meshes = n_meshes + 1
    end do

    allocate( mesh_base_names(n_meshes), stat=ierr, errmsg=err_msg )
    call check_err1(ierr, '(A)', trim(err_msg))

    i    = 0
    tmp1 = 1
    tmp2 = index(buffer, ',')-1
    if (tmp2 < tmp1) tmp2 = len_trim(buffer)
    do
       i = i + 1
       mesh_base_names(i) = buffer(tmp1:tmp2)
       tmp1 = min(tmp2+2, len_trim(buffer))
       tmp2 = tmp2 + index(buffer(tmp1:),',')
       if (tmp2 < tmp1) tmp2 = len_trim(buffer)
       if (tmp1 == tmp2) exit
    end do

    ! --------------------------------

    n_k = 10
    allocate(k_vect(n_k))

    call PetscOptionsGetIntArray(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, &
         & '-k', k_vect, n_k, set, ierr); CHKERRQ(ierr)
    if (.not. set) then
       ierr = 1
       call check_err1(ierr, '(A)', 'Please provide order of approximation')
    end if

    ! --------------------------------

    call PetscOptionsGetString(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, &
         & '-data', problem_data, set, ierr); CHKERRQ(ierr)
    if (.not. set) then
       ierr = 1
       call check_err1(ierr, '(A)', 'Please provide physical data')
    end if
    select case (trim(problem_data))
    case ('prism_1d')
       data_biot%sqrtm_permeability       => sqrtm_permeability_prism_1d
       data_biot%diff_sqrtm_permeability  => diff_sqrtm_permeability_prism_1d
       data_biot%source_fluid             => source_fluid_prism_1d
       data_biot%source_solid             => source_solid_prism_1d
       data_biot%dirichlet_fluid          => dirichlet_fluid_prism_1d
       data_biot%neumann_fluid            => neumann_fluid_prism_1d
       data_biot%ibc_fluid                => ibc_fluid_prism_1d_coupling
       data_biot%robin_fluid              => robin_fluid_prism_1d
       data_biot%dirichlet_solid          => dirichlet_solid_prism_1d
       data_biot%neumann_solid            => neumann_solid_prism_1d
       data_biot%initial_displacement     => initial_displacement_prism_1d
       data_biot%exact_total_stress       => exact_total_stress_prism_1d
       data_biot%exact_solid_displacement => exact_solid_displacement_prism_1d
       data_biot%exact_discharge_velocity => exact_discharge_velocity_prism_1d
       data_biot%exact_fluid_pressure     => exact_fluid_pressure_prism_1d_OS
       data_biot%bc_flags                 => bc_flags_prism_1d
       dim_ode = 3
    case ('prism_3d')
       data_biot%sqrtm_permeability       => sqrtm_permeability_prism_3d
       data_biot%diff_sqrtm_permeability  => diff_sqrtm_permeability_prism_3d
       data_biot%source_fluid             => source_fluid_prism_3d
       data_biot%source_solid             => source_solid_prism_3d
       data_biot%neumann_fluid            => neumann_fluid_prism_3d
       data_biot%ibc_fluid                => ibc_fluid_prism_3d_coupling
       data_biot%dirichlet_solid          => dirichlet_solid_prism_3d
       data_biot%neumann_solid            => neumann_solid_prism_3d
       data_biot%initial_displacement     => initial_displacement_prism_3d
       data_biot%exact_fluid_pressure     => exact_fluid_pressure_prism_1d_OS
       data_biot%bc_flags                 => bc_flags_prism_3d
       dim_ode = 3
    case default
       ierr = 1
       call check_err1(ierr, '(A)', 'Unknown physical data')
    end select

    call data_biot%set_from_options()

    ! --------------------------------

    call PetscOptionsGetReal(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, &
         & '-ts_t_start', t_start, set, ierr); CHKERRQ(ierr)
    if (.not. set) then
       ierr = 1
       call check_err1(ierr, '(A)', 'Please provide initial time')
    end if

    ! --------------------------------

    call PetscOptionsGetReal(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, &
         & '-ts_t_end', t_end, set, ierr); CHKERRQ(ierr)
    if (.not. set) then
       ierr = 1
       call check_err1(ierr, '(A)', 'Please provide final time')
    end if

    ! --------------------------------

    n_dt = 100
    allocate(dt(n_dt))

    call PetscOptionsGetRealArray(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, &
         & '-ts_dt', dt, n_dt, set, ierr); CHKERRQ(ierr)
    if (.not. set) then
       ierr = 1
       call check_err1(ierr, '(A)', 'Please provide time step(s)')
    end if

    ! --------------------------------

    call PetscOptionsGetString(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, &
         & '-ts_type', ts_type, set, ierr); CHKERRQ(ierr)
    if (.not. set) then
       ierr = 1
       call check_err1(ierr, '(A)', 'Please specify time stepping algorithm')
    end if

    ! --------------------------------
    
    call PetscOptionsGetString(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, &
         & '-exporter_filebasename', exporter_filebasename, set, ierr); CHKERRQ(ierr)
    if (.not. set) then
       ierr = 1
       call check_err1(ierr, '(A)', 'Please specify exporter filebasename')
    end if

    ! --------------------------------

    debug = .false.
    call PetscOptionsGetBool(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, &
         & '-debug', debug, set, ierr); CHKERRQ(ierr)

  end subroutine parse_cmdline

end program os_algo
