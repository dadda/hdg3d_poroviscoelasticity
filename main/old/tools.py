import shutil, os, sys, subprocess
import test_case_template
from sympy import *

def write_module(problem_type):

    if problem_type == 1:
        print '\nWriting a module with data for a stationary, linear problem...\n'
        is_permeability_coupled = False

    elif problem_type == 2:
        print '\nWriting a module with data for a stationary, nonlinear problem...\n'
        is_permeability_coupled = True

    elif problem_type == 3:
        print '\nWriting a module with data for a time dependent, linear problem...\n'
        is_permeability_coupled = False

    else:
        print '\nWriting a module with data for a time dependent, nonlinear problem...\n'
        is_permeability_coupled = True


    module_src = test_case_template.module_string

    basefilename = raw_input('\nEnter file name of the test case module: ')
    filename = basefilename + '.F90'
    f = open(filename, 'w')

    ###### Specify test case data

    ### Destdir
    destdir = raw_input('\nEnter name of destination directory: ')

    ### Meshfiles
    meshfiles_basename = raw_input('\nEnter absolute path of the meshfiles basename: ')

    ### Permeability

    if is_permeability_coupled:
        coupled_permeability = '.True.'
        min_poro = raw_input('\nMinimum porosity allowed: ')
        max_poro = raw_input('\nMaximum porosity allowed: ')
        initial_poro = raw_input('\nInitial porosity: ')
        mu_fl        = raw_input('\nFluid dynamic viscosity: ')
        coupled_k = \
            raw_input('\nEnter expression of the permeability coefficient. ' \
                             'Use symbols "phi, this%[parameter name]": ')

        # In this case this field is not used, so it could be set to whatever (scalar) real
        # expression
        uncoupled_k = '0.0d+0'
    else:
        coupled_permeability = '.False.'
        min_poro = '0.0d+0'
        max_poro = '0.0d+0'
        initial_poro = '0.1567d+0'
        mu_fl = '1.0d+0'
        coupled_k = '0.0d+0'

        uncoupled_k = \
            raw_input('\nEnter expression of the permeability coefficient. ' \
                             'Use symbols "x, y, z, t, this%[parameter name]": ')

    ### Characteristic scales
    print '\nCharacteristic scales. Use this%[parameter name] when needed:'
    x_char = raw_input('\nCharacteristic length: ')
    t_char = raw_input('\nCharacteristic time: ')
    u_char = raw_input('\nCharacteristic displacement: ')
    p_char = raw_input('\nCharacterisitc fluid pressure: ')
    s_char = raw_input('\nCharacteristic stress: ')
    v_char = raw_input('\nCharacteristic Darcy velocity: ')

    ### Viscoelastic coefficients

    print '\nEnter expression of the viscoelastic coefficients. ' \
            'Use symbols "x, y, z, t, this%[parameter name]".'
    mu_e = raw_input('\nElastic shear modulus: ')
    lambda_e = raw_input('\nElastic lambda: ')
    delta = raw_input('\nViscous amount delta: ')
    mu_v = raw_input('\nViscous shear modulus: ')
    lambda_v = raw_input('\nViscous lambda: ')

    ### Exact solution - at present it only works if k does not depend on parameters

    with_exact_solution = raw_input('\nShall an exact solution be tested ("y" or "n")? ')
    if with_exact_solution == 'y':
        ux = raw_input('\nEnter expression of the x-component of the solid displacement. ' \
                       'Use symbols "x, y, z, t": ')
        uy = raw_input('\nEnter expression of the y-component of the solid displacement. ' \
                       'Use symbols "x, y, z, t": ')
        uz = raw_input('\nEnter expression of the z-component of the solid displacement. ' \
                       'Use symbols "x, y, z, t": ')
        p = raw_input('\nExact fluid pressure. Use symbols "x, y, z, t": ')
        k = raw_input('\nPermeability coefficient. Use symbols "x, y, z, t": ')

        # Use symbolic calculus to compute the source terms

        x, y, z, t, T = symbols('x y z t T')

        p_expr  = sympify(p)
        ux_expr = sympify(ux)
        uy_expr = sympify(uy)
        uz_expr = sympify(uz)
        k_expr  = sympify(k)

        vx_expr = simplify( -k_expr*diff(p_expr,x) )
        vy_expr = simplify( -k_expr*diff(p_expr,y) )
        vz_expr = simplify( -k_expr*diff(p_expr,z) )

        vx = str(vx_expr)
        vy = str(vy_expr)
        vz = str(vz_expr)

        exx = diff(ux_expr,x)
        eyy = diff(uy_expr,y)
        ezz = diff(uz_expr,z)
        exy = 0.5 * (diff(ux_expr,y) + diff(uy_expr,x))
        exz = 0.5 * (diff(ux_expr,z) + diff(uz_expr,x))
        eyz = 0.5 * (diff(uy_expr,z) + diff(uz_expr,y))

        exxt = diff(exx,t)
        eyyt = diff(eyy,t)
        ezzt = diff(ezz,t)
        exyt = diff(exy,t)
        exzt = diff(exz,t)
        eyzt = diff(eyz,t)

        flag = raw_input('\nDo you need to overload the routine for computing the fluid ' \
                         'contribution to the right hand side in the local solvers (y/n)? ')
        if flag == 'y':
            overload_set_fluid_source_term = '.True.'
            source_fluid = '-1.0d+100'
            Ff = 'Ff = UpdateMe'
        else:
            overload_set_fluid_source_term = '.False.'
            source_fluid_expr = simplify( exxt + eyyt + ezzt + \
                                          diff(vx_expr,x) + diff(vy_expr,y) + diff(vz_expr,z) )
            source_fluid = str(source_fluid_expr)
            Ff = ''

        # Compute total stress
        mu_e_expr = sympify(mu_e)
        mu_v_expr = sympify(mu_v)
        lambda_e_expr = sympify(lambda_e)
        lambda_v_expr = sympify(lambda_v)
        delta_expr = sympify(delta)

        Txx_expr = simplify( 2*mu_e_expr*exx + lambda_e_expr*(exx+eyy+ezz) + \
                             delta_expr * (2*mu_v_expr*exxt + lambda_v_expr*(exxt+eyyt+ezzt)) - \
                             p_expr )
        Tyy_expr = simplify( 2*mu_e_expr*eyy + lambda_e_expr*(exx+eyy+ezz) + \
                             delta_expr * (2*mu_v_expr*eyyt + lambda_v_expr*(exxt+eyyt+ezzt)) - \
                             p_expr )
        Tzz_expr = simplify( 2*mu_e_expr*ezz + lambda_e_expr*(exx+eyy+ezz) + \
                             delta_expr * (2*mu_v_expr*ezzt + lambda_v_expr*(exxt+eyyt+ezzt)) - \
                             p_expr )
        Txy_expr = simplify( 2*mu_e_expr*exy + delta_expr * (2*mu_v_expr*exyt) )
        Txz_expr = simplify( 2*mu_e_expr*exz + delta_expr * (2*mu_v_expr*exzt) )
        Tyz_expr = simplify( 2*mu_e_expr*eyz + delta_expr * (2*mu_v_expr*eyzt) )
        Txx = str(Txx_expr)
        Tyy = str(Tyy_expr)
        Tzz = str(Tzz_expr)
        Txy = str(Txy_expr)
        Txz = str(Txz_expr)
        Tyz = str(Tyz_expr)

        source_solid_x_expr = simplify( diff(Txx_expr,x) + diff(Txy_expr,y) + diff(Txz_expr,z) )
        source_solid_y_expr = simplify( diff(Txy_expr,x) + diff(Tyy_expr,y) + diff(Tyz_expr,z) )
        source_solid_z_expr = simplify( diff(Txz_expr,x) + diff(Tyz_expr,y) + diff(Tzz_expr,z) )
        source_solid_x = str(source_solid_x_expr)
        source_solid_y = str(source_solid_y_expr)
        source_solid_z = str(source_solid_z_expr)

        #######################
        # Boundary conditions #
        #######################

        flag = raw_input('\nDo you want to load physical boundary flags (y/n)? ')
        if flag == 'y':
            physical_flags = '.True.'
        else:
            physical_flags = '.False.'

        # Dirichlet

        gDf = p
        flag = raw_input('\nDo you want to impose standard Dirichlet BCs on u (y/n)? ')
        if flag == 'y':
            gDs1 = ux
            gDs2 = uy
            gDs3 = uz
        else:
            gDs1 = '-1.0d+100'
            gDs2 = '-1.0d+100'
            gDs3 = '-1.0d+100'

        # Neumann

        flag = raw_input('\nDo you want to impose standard Neumann BCs on the fluid (y/n)? ')
        if flag == 'y':
            # In this case in which we assign the exact solution, we use the format
            # v . n = gNf . n to specify the boundary conditions

            dim_Nf = '3'
            gNf1 = 'gNf(:,:,1) = (%s) * (1.0d+0/this%%v_char)' % vx
            gNf2 = 'gNf(:,:,2) = (%s) * (1.0d+0/this%%v_char)' % vy
            gNf3 = 'gNf(:,:,3) = (%s) * (1.0d+0/this%%v_char)' % vz
        else:
            dim_Nf = '0'
            gNf1 = ''
            gNf2 = ''
            gNf3 = ''

        flag = raw_input('\nDo you want to impose Neumann BCs on the solid (y/n)? ')
        if flag == 'y':
            # In this case in which we assign the exact solution, we use the format
            # sigma n = gNs n to specify the boundary conditions

            dim_Ns = '6'
            gNs1 = 'gNs(:,:,1) = (%s) * (1.0d+0/this%%s_char)' % Txx
            gNs2 = 'gNs(:,:,2) = (%s) * (1.0d+0/this%%s_char)' % Tyy
            gNs3 = 'gNs(:,:,3) = (%s) * (1.0d+0/this%%s_char)' % Tzz
            gNs4 = 'gNs(:,:,4) = (%s) * (1.0d+0/this%%s_char)' % Txy
            gNs5 = 'gNs(:,:,5) = (%s) * (1.0d+0/this%%s_char)' % Txz
            gNs6 = 'gNs(:,:,6) = (%s) * (1.0d+0/this%%s_char)' % Tyz

        else:
            dim_Ns = '0'
            gNs1 = ''
            gNs2 = ''
            gNs3 = ''
            gNs4 = ''
            gNs5 = ''
            gNs6 = ''

        flag = raw_input('\nDo you want to impose Diri on x,y - Neu on z BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            # In this case in which we assign the exact solution, we use the format
            # sigma n = gNs n to specify the boundary conditions
            gD_DirNeu_xy_1  = ux
            gD_DirNeu_xy_2  = uy

            dim_DirNeu_xy = '3'
            gN_DirNeu_xy_1 = 'gNs(:,:,1) = (%s) * (1.0d+0/this%%s_char)' % Tzz
            gN_DirNeu_xy_2 = 'gNs(:,:,2) = (%s) * (1.0d+0/this%%s_char)' % Txz
            gN_DirNeu_xy_3 = 'gNs(:,:,3) = (%s) * (1.0d+0/this%%s_char)' % Tyz

        else:
            gD_DirNeu_xy_1  = '-1.0d+100'
            gD_DirNeu_xy_2  = '-1.0d+100'

            dim_DirNeu_xy = '0'
            gN_DirNeu_xy_1 = ''
            gN_DirNeu_xy_2 = ''
            gN_DirNeu_xy_3 = ''

        flag = raw_input('\nDo you want to impose Diri on x,z - Neu on y BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            # In this case in which we assign the exact solution, we use the format
            # sigma n = gNs n to specify the boundary conditions
            gD_DirNeu_xz_1  = ux
            gD_DirNeu_xz_2  = uz

            dim_DirNeu_xz = '3'
            gN_DirNeu_xz_1 = 'gNs(:,:,1) = (%s) * (1.0d+0/this%%s_char)' % Tyy
            gN_DirNeu_xz_2 = 'gNs(:,:,2) = (%s) * (1.0d+0/this%%s_char)' % Txy
            gN_DirNeu_xz_3 = 'gNs(:,:,3) = (%s) * (1.0d+0/this%%s_char)' % Tyz

        else:
            gD_DirNeu_xz_1  = '-1.0d+100'
            gD_DirNeu_xz_2  = '-1.0d+100'

            dim_DirNeu_xz = '0'
            gN_DirNeu_xz_1 = ''
            gN_DirNeu_xz_2 = ''
            gN_DirNeu_xz_3 = ''

        flag = raw_input('\nDo you want to impose Diri on y,z - Neu on x BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            # In this case in which we assign the exact solution, we use the format
            # sigma n = gNs n to specify the boundary conditions
            gD_DirNeu_yz_1  = uy
            gD_DirNeu_yz_2  = uz

            dim_DirNeu_yz = '3'
            gN_DirNeu_yz_1 = 'gNs(:,:,1) = (%s) * (1.0d+0/this%%s_char)' % Txx
            gN_DirNeu_yz_2 = 'gNs(:,:,2) = (%s) * (1.0d+0/this%%s_char)' % Txy
            gN_DirNeu_yz_3 = 'gNs(:,:,3) = (%s) * (1.0d+0/this%%s_char)' % Txz

        else:
            gD_DirNeu_yz_1  = '-1.0d+100'
            gD_DirNeu_yz_2  = '-1.0d+100'

            dim_DirNeu_yz = '0'
            gN_DirNeu_yz_1 = ''
            gN_DirNeu_yz_2 = ''
            gN_DirNeu_yz_3 = ''

        flag = raw_input('\nDo you want to impose Diri on x - Neu on y,z BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            # In this case in which we assign the exact solution, we use the format
            # sigma n = gNs n to specify the boundary conditions
            gD_DirNeu_x  = ux

            dim_DirNeu_x = '5'
            gN_DirNeu_x_1 = 'gNs(:,:,1) = (%s) * (1.0d+0/this%%s_char)' % Tyy
            gN_DirNeu_x_2 = 'gNs(:,:,2) = (%s) * (1.0d+0/this%%s_char)' % Tzz
            gN_DirNeu_x_3 = 'gNs(:,:,3) = (%s) * (1.0d+0/this%%s_char)' % Txy
            gN_DirNeu_x_4 = 'gNs(:,:,4) = (%s) * (1.0d+0/this%%s_char)' % Txz
            gN_DirNeu_x_5 = 'gNs(:,:,5) = (%s) * (1.0d+0/this%%s_char)' % Tyz

        else:
            gD_DirNeu_x  = '-1.0d+100'

            dim_DirNeu_x = '0'
            gN_DirNeu_x_1 = ''
            gN_DirNeu_x_2 = ''
            gN_DirNeu_x_3 = ''
            gN_DirNeu_x_4 = ''
            gN_DirNeu_x_5 = ''

        flag = raw_input('\nDo you want to impose Diri on y - Neu on x,z BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            # In this case in which we assign the exact solution, we use the format
            # sigma n = gNs n to specify the boundary conditions
            gD_DirNeu_y  = uy

            dim_DirNeu_y = '5'
            gN_DirNeu_y_1 = 'gNs(:,:,1) = (%s) * (1.0d+0/this%%s_char)' % Txx
            gN_DirNeu_y_2 = 'gNs(:,:,2) = (%s) * (1.0d+0/this%%s_char)' % Tzz
            gN_DirNeu_y_3 = 'gNs(:,:,3) = (%s) * (1.0d+0/this%%s_char)' % Txy
            gN_DirNeu_y_4 = 'gNs(:,:,4) = (%s) * (1.0d+0/this%%s_char)' % Txz
            gN_DirNeu_y_5 = 'gNs(:,:,5) = (%s) * (1.0d+0/this%%s_char)' % Tyz

        else:
            gD_DirNeu_y  = '-1.0d+100'

            dim_DirNeu_y = '0'
            gN_DirNeu_y_1 = ''
            gN_DirNeu_y_2 = ''
            gN_DirNeu_y_3 = ''
            gN_DirNeu_y_4 = ''
            gN_DirNeu_y_5 = ''

        flag = raw_input('\nDo you want to impose Diri on z - Neu on x,y BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            # In this case in which we assign the exact solution, we use the format
            # sigma n = gNs n to specify the boundary conditions
            gD_DirNeu_z  = uz

            dim_DirNeu_z = '5'
            gN_DirNeu_z_1 = 'gNs(:,:,1) = (%s) * (1.0d+0/this%%s_char)' % Txx
            gN_DirNeu_z_2 = 'gNs(:,:,2) = (%s) * (1.0d+0/this%%s_char)' % Tyy
            gN_DirNeu_z_3 = 'gNs(:,:,3) = (%s) * (1.0d+0/this%%s_char)' % Txy
            gN_DirNeu_z_4 = 'gNs(:,:,4) = (%s) * (1.0d+0/this%%s_char)' % Txz
            gN_DirNeu_z_5 = 'gNs(:,:,5) = (%s) * (1.0d+0/this%%s_char)' % Tyz

        else:
            gD_DirNeu_z  = '-1.0d+100'

            dim_DirNeu_z = '0'
            gN_DirNeu_z_1 = ''
            gN_DirNeu_z_2 = ''
            gN_DirNeu_z_3 = ''
            gN_DirNeu_z_4 = ''
            gN_DirNeu_z_5 = ''
        
    else:
        Txx = '0.0d+0'
        Tyy = '0.0d+0'
        Tzz = '0.0d+0'
        Txy = '0.0d+0'
        Txz = '0.0d+0'
        Tyz = '0.0d+0'
        ux = '0.0d+0'
        uy = '0.0d+0'
        uz = '0.0d+0'
        vx = '0.0d+0'
        vy = '0.0d+0'
        vz = '0.0d+0'
        p = '0.0d+0'

        flag = raw_input('\nDo you need to overload the routine for computing the fluid ' \
                         'contribution to the right hand side in the local solvers (y/n)? ')
        if flag == 'y':
            overload_set_fluid_source_term = '.True.'
            source_fluid = '-1.0d+100'
            Ff = 'Ff = UpdateMe'
        else:
            overload_set_fluid_source_term = '.False.'
            source_fluid = \
                           raw_input('\nEnter expression of the fluid source term. ' \
                                     'Use symbols "x, y, z, t, this%[parameter name]": ')
            Ff = ''

        source_solid_x = \
            raw_input('\nEnter expression of the x component of the solid source term. ' \
                      'Use symbols "x, y, z, t, this%[parameter name]": ')

        source_solid_y = \
            raw_input('\nEnter expression of the y component of the solid source term. ' \
                      'Use symbols "x, y, z, t, this%[parameter name]": ')

        source_solid_z = \
            raw_input('\nEnter expression of the z component of the solid source term. ' \
                      'Use symbols "x, y, z, t, this%[parameter name]": ')

        #######################
        # Boundary conditions #
        #######################

        flag = raw_input('\nDo you want to load physical boundary flags (y/n)? ')
        if flag == 'y':
            physical_flags = '.True.'
        else:
            physical_flags = '.False.'

        gDf = raw_input('\nEnter expression of the Dirichlet BC for the fluid. ' \
                        'Use symbols "x, y, z, t, this%[parameter name]": ')

        flag = raw_input('\nDo you want to impose standard Dirichlet BCs on u (y/n)? ')
        if flag == 'y':
            gDs1 = raw_input('\nEnter expression of the x component of the Dirichlet BC ' \
                             'for the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
            gDs2 = raw_input('\nEnter expression of the y component of the Dirichlet BC ' \
                             'for the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
            gDs3 = raw_input('\nEnter expression of the z component of the Dirichlet BC ' \
                             'for the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
        else:
            gDs1 = '-1.0d+100'
            gDs2 = '-1.0d+100'
            gDs3 = '-1.0d+100'


        flag = raw_input('\nDo you need to impose Neumann BCs on the fluid (y/n)? ')
        if flag == 'y':
            bctype = raw_input('\nChoose how you want to specify Neumann BC for the fluid:'\
                               '\n(1) v . n = gNf' \
                               '\n(2) v . n = gNf . n\n\nChoice: ')
            if bctype == '1':
                dim_Nf = '1'
                gNf1 = \
                       raw_input('\nEnter expression of the Neumann BC for the fluid. ' \
                                 'Use symbols "x, y, z, t, this%[parameter name]": ')
                gNf1 = 'gNf(:,:,1) = (' + gNf1 + ') * (1.0d+0/this%%v_char)'
                gNf2 = ''
                gNf3 = ''
            else:
                dim_Nf = '3'
                gNf1 = \
                  raw_input('\nEnter expression of x component of the Neumann BC for the fluid. ' \
                              'Use symbols "x, y, z, t, this%[parameter name]": ')
                gNf1 = 'gNf(:,:,1) = (' + gNf1 + ') * (1.0d+0/this%%v_char)'
                gNf2 = \
                  raw_input('\nEnter expression of y component of the Neumann BC for the fluid. ' \
                              'Use symbols "x, y, z, t, this%[parameter name]": ')
                gNf2 = 'gNf(:,:,2) = (' + gNf2 + ') * (1.0d+0/this%%v_char)'
                gNf3 = \
                  raw_input('\nEnter expression of z component of the Neumann BC for the fluid. ' \
                              'Use symbols "x, y, z, t, this%[parameter name]": ')
                gNf3 = 'gNf(:,:,3) = (' + gNf3 + ') * (1.0d+0/this%%v_char)'
        else:
            dim_Nf = '0'
            gNf1 = ''
            gNf2 = ''
            gNf3 = ''

        # Solid BCs

        flag = raw_input('\nDo you want to impose standard Neumann BCs on the solid (y/n)? ')
        if flag == 'y':
            bctype = raw_input('\nChoose how you want to specify Neumann BC on the solid:'\
                               '\n(1) sigma n = gNs' \
                               '\n(2) sigma n = gNs n\n\nChoice: ')
            if bctype == '1':
                dim_Ns = '3'
                gNs1 = \
                  raw_input('\nEnter expression of the x component of Neumann BC' \
                            ' for the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
                gNs1 = 'gNs(:,:,1) = (' + gNs1 + ') * (1.0d+0/this%%s_char)'
                gNs2 = \
                  raw_input('\nEnter expression of the y component of Neumann BC' \
                            ' for the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
                gNs2 = 'gNs(:,:,2) = (' + gNs2 + ') * (1.0d+0/this%%s_char)'
                gNs3 = \
                  raw_input('\nEnter expression of the z component of Neumann BC' \
                            ' for the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
                gNs3 = 'gNs(:,:,3) = (' + gNs3 + ') * (1.0d+0/this%%s_char)'
                gNs4 = ''
                gNs5 = ''
                gNs6 = ''
            else:
                dim_Ns = '6'
                gNs1 = \
                       raw_input('\nEnter expression of (x,x) component of the Neumann BC on ' \
                                 'the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
                gNs1 = 'gNs(:,:,1) = (' + gNs1 + ') * (1.0d+0/this%%s_char)'
                gNs2 = \
                       raw_input('\nEnter expression of (y,y) component of the Neumann BC on ' \
                                 'the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
                gNs2 = 'gNs(:,:,2) = (' + gNs2 + ') * (1.0d+0/this%%s_char)'
                gNs3 = \
                       raw_input('\nEnter expression of (z,z) component of the Neumann BC on ' \
                                 'the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
                gNs3 = 'gNs(:,:,3) = (' + gNs3 + ') * (1.0d+0/this%%s_char)'
                gNs4 = \
                       raw_input('\nEnter expression of (x,y) component of the Neumann BC on ' \
                                 'the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
                gNs4 = 'gNs(:,:,4) = (' + gNs4 + ') * (1.0d+0/this%%s_char)'
                gNs5 = \
                       raw_input('\nEnter expression of (x,z) component of the Neumann BC on ' \
                                 'the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
                gNs5 = 'gNs(:,:,5) = (' + gNs5 + ') * (1.0d+0/this%%s_char)'
                gNs6 = \
                       raw_input('\nEnter expression of (y,z) component of the Neumann BC on ' \
                                 'the solid. Use symbols "x, y, z, t, this%[parameter name]": ')
                gNs6 = 'gNs(:,:,6) = (' + gNs6 + ') * (1.0d+0/this%%s_char)'
        else:
            dim_Ns = '0'
            gNs1 = ''
            gNs2 = ''
            gNs3 = ''
            gNs4 = ''
            gNs5 = ''
            gNs6 = ''

        flag = raw_input('\nDo you want to impose Diri on x,y - Neumann on z BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            gD_DirNeu_xy_1 = raw_input('\nEnter expression of the x component of ' \
                                       'mixed Dirichlet BCs. Use symbols ' \
                                       '"x, y, z, t, this%[parameter name]": ')
            gD_DirNeu_xy_2 = raw_input('\nEnter expression of the y component of ' \
                                       'mixed Dirichlet BCs. Use symbols ' \
                                       '"x, y, z, t, this%[parameter name]": ')

            bctype = raw_input('\nChoose how you want to specify Neumann BC in the z direction: '\
                               '\n(1) sigma(3,:) \cdot n = gN_DirNeu_xy' \
                               '\n(2) sigma(3,:) \cdot n = gN_DirNeu_xy \cdot n\n\nChoice: ')
            if bctype == '1':
                dim_DirNeu_xy = '1'
                gN_DirNeu_xy_1 = raw_input('\nEnter expression of the z component of ' \
                                           'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_xy_1 = 'gNs(:,:,1) = (' + gN_DirNeu_xy_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_xy_2 = ''
                gN_DirNeu_xy_3 = ''

            else:
                dim_DirNeu_xy = '3'
                gN_DirNeu_xy_1 = raw_input('\nEnter expression of the (z,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_xy_1 = 'gNs(:,:,1) = (' + gN_DirNeu_xy_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_xy_2 = raw_input('\nEnter expression of the (x,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_xy_2 = 'gNs(:,:,2) = (' + gN_DirNeu_xy_2 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_xy_3 = raw_input('\nEnter expression of the (y,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_xy_3 = 'gNs(:,:,3) = (' + gN_DirNeu_xy_3 + ') * (1.0d+0/this%%s_char)'
        else:
            gD_DirNeu_xy_1  = '-1.0d+100'
            gD_DirNeu_xy_2  = '-1.0d+100'

            dim_DirNeu_xy = '0'
            gN_DirNeu_xy_1 = ''
            gN_DirNeu_xy_2 = ''
            gN_DirNeu_xy_3 = ''
            

        #########################

        flag = raw_input('\nDo you want to impose Diri on x,z - Neumann on y BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            gD_DirNeu_xz_1 = raw_input('\nEnter expression of the x component of ' \
                                       'mixed Dirichlet BCs. Use symbols ' \
                                       '"x, y, z, t, this%[parameter name]": ')
            gD_DirNeu_xz_2 = raw_input('\nEnter expression of the z component of ' \
                                       'mixed Dirichlet BCs. Use symbols ' \
                                       '"x, y, z, t, this%[parameter name]": ')

            bctype = raw_input('\nChoose how you want to specify Neumann BC in the y direction: '\
                               '\n(1) sigma(2,:) \cdot n = gN_DirNeu_xz' \
                               '\n(2) sigma(2,:) \cdot n = gN_DirNeu_xz \cdot n\n\nChoice: ')
            if bctype == '1':
                dim_DirNeu_xz = '1'
                gN_DirNeu_xz_1 = raw_input('\nEnter expression of the y component of ' \
                                           'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_xz_1 = 'gNs(:,:,1) = (' + gN_DirNeu_xz_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_xz_2 = ''
                gN_DirNeu_xz_3 = ''

            else:
                dim_DirNeu_xz = '3'
                gN_DirNeu_xz_1 = raw_input('\nEnter expression of the (y,y) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_xz_1 = 'gNs(:,:,1) = (' + gN_DirNeu_xz_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_xz_2 = raw_input('\nEnter expression of the (x,y) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_xz_2 = 'gNs(:,:,2) = (' + gN_DirNeu_xz_2 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_xz_3 = raw_input('\nEnter expression of the (y,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_xz_3 = 'gNs(:,:,3) = (' + gN_DirNeu_xz_3 + ') * (1.0d+0/this%%s_char)'
        else:
            gD_DirNeu_xz_1  = '-1.0d+100'
            gD_DirNeu_xz_2  = '-1.0d+100'

            dim_DirNeu_xz = '0'
            gN_DirNeu_xz_1 = ''
            gN_DirNeu_xz_2 = ''
            gN_DirNeu_xz_3 = ''


        #########################

        flag = raw_input('\nDo you want to impose Diri on y,z - Neumann on x BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            gD_DirNeu_yz_1 = raw_input('\nEnter expression of the y component of ' \
                                       'mixed Dirichlet BCs. Use symbols ' \
                                       '"x, y, z, t, this%[parameter name]": ')
            gD_DirNeu_yz_2 = raw_input('\nEnter expression of the z component of ' \
                                       'mixed Dirichlet BCs. Use symbols ' \
                                       '"x, y, z, t, this%[parameter name]": ')

            bctype = raw_input('\nChoose how you want to specify Neumann BC in the x direction: '\
                               '\n(1) sigma(1,:) \cdot n = gN_DirNeu_yz' \
                               '\n(2) sigma(1,:) \cdot n = gN_DirNeu_yz \cdot n\n\nChoice: ')
            if bctype == '1':
                dim_DirNeu_yz = '1'
                gN_DirNeu_yz_1 = raw_input('\nEnter expression of the x component of ' \
                                           'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_yz_1 = 'gNs(:,:,1) = (' + gN_DirNeu_yz_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_yz_2 = ''
                gN_DirNeu_yz_3 = ''

            else:
                dim_DirNeu_yz = '3'
                gN_DirNeu_yz_1 = raw_input('\nEnter expression of the (x,x) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_yz_1 = 'gNs(:,:,1) = (' + gN_DirNeu_yz_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_yz_2 = raw_input('\nEnter expression of the (x,y) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_yz_2 = 'gNs(:,:,2) = (' + gN_DirNeu_yz_2 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_yz_3 = raw_input('\nEnter expression of the (x,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_yz_3 = 'gNs(:,:,3) = (' + gN_DirNeu_yz_3 + ') * (1.0d+0/this%%s_char)'
        else:
            gD_DirNeu_yz_1  = '-1.0d+100'
            gD_DirNeu_yz_2  = '-1.0d+100'

            dim_DirNeu_yz = '0'
            gN_DirNeu_yz_1 = ''
            gN_DirNeu_yz_2 = ''
            gN_DirNeu_yz_3 = ''


        #########################

        flag = raw_input('\nDo you want to impose Diri on x - Neumann on y,z BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            gD_DirNeu_x = raw_input('\nEnter expression of the x component of ' \
                                       'mixed Dirichlet BCs. Use symbols ' \
                                       '"x, y, z, t, this%[parameter name]": ')

            bctype = raw_input('\nChoose how you want to specify Neumann BC in the y,z plane: '\
                               '\n(1) sigma(2:3,:) \cdot n = gN_DirNeu_x' \
                               '\n(2) sigma(2:3,:) \cdot n = gN_DirNeu_x \cdot n\n\nChoice: ')
            if bctype == '1':
                dim_DirNeu_x = '2'
                gN_DirNeu_x_1 = raw_input('\nEnter expression of the y component of ' \
                                           'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_x_1 = 'gNs(:,:,1) = (' + gN_DirNeu_x_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_x_2 = raw_input('\nEnter expression of the z component of ' \
                                           'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_x_2 = 'gNs(:,:,2) = (' + gN_DirNeu_x_2 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_x_3 = ''
                gN_DirNeu_x_4 = ''
                gN_DirNeu_x_5 = ''

            else:
                dim_DirNeu_x = '5'
                gN_DirNeu_x_1 = raw_input('\nEnter expression of the (y,y) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_x_1 = 'gNs(:,:,1) = (' + gN_DirNeu_x_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_x_2 = raw_input('\nEnter expression of the (z,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_x_2 = 'gNs(:,:,2) = (' + gN_DirNeu_x_2 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_x_3 = raw_input('\nEnter expression of the (x,y) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_x_3 = 'gNs(:,:,3) = (' + gN_DirNeu_x_3 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_x_4 = raw_input('\nEnter expression of the (x,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_x_4 = 'gNs(:,:,4) = (' + gN_DirNeu_x_4 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_x_5 = raw_input('\nEnter expression of the (y,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_x_5 = 'gNs(:,:,5) = (' + gN_DirNeu_x_5 + ') * (1.0d+0/this%%s_char)'
        else:
            gD_DirNeu_x  = '-1.0d+100'

            dim_DirNeu_x = '0'
            gN_DirNeu_x_1 = ''
            gN_DirNeu_x_2 = ''
            gN_DirNeu_x_3 = ''
            gN_DirNeu_x_4 = ''
            gN_DirNeu_x_5 = ''

        #########################

        flag = raw_input('\nDo you want to impose Diri on y - Neumann on x,z BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            gD_DirNeu_y = raw_input('\nEnter expression of the y component of ' \
                                       'mixed Dirichlet BCs. Use symbols ' \
                                       '"x, y, z, t, this%[parameter name]": ')

            bctype = raw_input('\nChoose how you want to specify Neumann BC in the x,z plane: '\
                               '\n(1) sigma([1,3],:) \cdot n = gN_DirNeu_y' \
                               '\n(2) sigma([1,3],:) \cdot n = gN_DirNeu_y \cdot n\n\nChoice: ')
            if bctype == '1':
                dim_DirNeu_y = '2'
                gN_DirNeu_y_1 = raw_input('\nEnter expression of the x component of ' \
                                           'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_y_1 = 'gNs(:,:,1) = (' + gN_DirNeu_y_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_y_2 = raw_input('\nEnter expression of the z component of ' \
                                           'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_y_2 = 'gNs(:,:,2) = (' + gN_DirNeu_y_2 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_y_3 = ''
                gN_DirNeu_y_4 = ''
                gN_DirNeu_y_5 = ''

            else:
                dim_DirNeu_y = '5'
                gN_DirNeu_y_1 = raw_input('\nEnter expression of the (x,x) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_y_1 = 'gNs(:,:,1) = (' + gN_DirNeu_y_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_y_2 = raw_input('\nEnter expression of the (z,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_y_2 = 'gNs(:,:,2) = (' + gN_DirNeu_y_2 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_y_3 = raw_input('\nEnter expression of the (x,y) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_y_3 = 'gNs(:,:,3) = (' + gN_DirNeu_y_3 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_y_4 = raw_input('\nEnter expression of the (x,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_y_4 = 'gNs(:,:,4) = (' + gN_DirNeu_y_4 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_y_5 = raw_input('\nEnter expression of the (y,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_y_5 = 'gNs(:,:,5) = (' + gN_DirNeu_y_5 + ') * (1.0d+0/this%%s_char)'
        else:
            gD_DirNeu_y  = '-1.0d+100'

            dim_DirNeu_y = '0'
            gN_DirNeu_y_1 = ''
            gN_DirNeu_y_2 = ''
            gN_DirNeu_y_3 = ''
            gN_DirNeu_y_4 = ''
            gN_DirNeu_y_5 = ''

        #########################

        flag = raw_input('\nDo you want to impose Diri on z - Neumann on x,y BCs ' \
                         'on the solid (y/n)? ')
        if flag == 'y':
            gD_DirNeu_z = raw_input('\nEnter expression of the z component of ' \
                                       'mixed Dirichlet BCs. Use symbols ' \
                                       '"x, y, z, t, this%[parameter name]": ')

            bctype = raw_input('\nChoose how you want to specify Neumann BC in the x,y plane: '\
                               '\n(1) sigma(1:2,:) \cdot n = gN_DirNeu_z' \
                               '\n(2) sigma(1:2,:) \cdot n = gN_DirNeu_z \cdot n\n\nChoice: ')
            if bctype == '1':
                dim_DirNeu_z = '2'
                gN_DirNeu_z_1 = raw_input('\nEnter expression of the x component of ' \
                                           'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_z_1 = 'gNs(:,:,1) = (' + gN_DirNeu_z_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_z_2 = raw_input('\nEnter expression of the y component of ' \
                                           'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_z_2 = 'gNs(:,:,2) = (' + gN_DirNeu_z_2 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_z_3 = ''
                gN_DirNeu_z_4 = ''
                gN_DirNeu_z_5 = ''

            else:
                dim_DirNeu_z = '5'
                gN_DirNeu_z_1 = raw_input('\nEnter expression of the (x,x) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_z_1 = 'gNs(:,:,1) = (' + gN_DirNeu_z_1 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_z_2 = raw_input('\nEnter expression of the (y,y) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_z_2 = 'gNs(:,:,2) = (' + gN_DirNeu_z_2 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_z_3 = raw_input('\nEnter expression of the (x,y) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_z_3 = 'gNs(:,:,3) = (' + gN_DirNeu_z_3 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_z_4 = raw_input('\nEnter expression of the (x,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_z_4 = 'gNs(:,:,4) = (' + gN_DirNeu_z_4 + ') * (1.0d+0/this%%s_char)'
                gN_DirNeu_z_5 = raw_input('\nEnter expression of the (y,z) component of ' \
                                    'mixed Neumann BCs. Use symbols ' \
                                    '"x, y, z, t, this%[parameter name]": ')
                gN_DirNeu_z_5 = 'gNs(:,:,5) = (' + gN_DirNeu_z_5 + ') * (1.0d+0/this%%s_char)'
        else:
            gD_DirNeu_z  = '-1.0d+100'

            dim_DirNeu_z = '0'
            gN_DirNeu_z_1 = ''
            gN_DirNeu_z_2 = ''
            gN_DirNeu_z_3 = ''
            gN_DirNeu_z_4 = ''
            gN_DirNeu_z_5 = ''

    ### Initial condition

    if problem_type == 1 or problem_type == 2:
        Tstart = '0.0d+0'
        Tend   = '-1.0d+0'
        nsteps = '1'
        ux0    = '0.0d+0'
        uy0    = '0.0d+0'
        uz0    = '0.0d+0'
        exx0   = '0.0d+0'
        eyy0   = '0.0d+0'
        ezz0   = '0.0d+0'
        exy0   = '0.0d+0'
        exz0   = '0.0d+0'
        eyz0   = '0.0d+0'
        divu0  = '0.0d+0'
    else:
        Tstart = raw_input('\nEnter initial time: ')
        Tend   = raw_input('\nEnter final time: ')
        nsteps = raw_input('\nEnter number of time steps: ')
        if with_exact_solution == 'y':
            ux0 = str( simplify(ux_expr.subs(t, 0)) )
            uy0 = str( simplify(uy_expr.subs(t, 0)) )
            uz0 = str( simplify(uz_expr.subs(t, 0)) )
            exx0 = str( simplify( exx.subs(t,0) ) )
            eyy0 = str( simplify( eyy.subs(t,0) ) )
            ezz0 = str( simplify( ezz.subs(t,0) ) )
            exy0 = str( simplify( exy.subs(t,0) ) )
            exz0 = str( simplify( exz.subs(t,0) ) )
            eyz0 = str( simplify( eyz.subs(t,0) ) )
            divu0 = str( simplify(exx.subs(t, 0) + eyy.subs(t, 0) + ezz.subs(t, 0)) )
        else:
            ux0 = raw_input('\nEnter expression of the x component of the initial condition ' \
                            'on the solid displacement. ' \
                            'Use symbols "x, y, z, t, this%[parameter_name]": ')
            uy0 = raw_input('\nEnter expression of the y component of the initial condition ' \
                            'on the solid displacement. ' \
                            'Use symbols "x, y, z, t, this%[parameter_name]": ')
            uz0 = raw_input('\nEnter expression of the z component of the initial condition ' \
                            'on the solid displacement. ' \
                            'Use symbols "x, y, z, t, this%[parameter_name]": ')
            exx0 = raw_input('\nEnter expression of the xx component of the infinitesimal ' \
                            'strain tensor. ' \
                            'Use symbols "x, y, z, t, this%[parameter_name]": ')
            eyy0 = raw_input('\nEnter expression of the yy component of the infinitesimal ' \
                            'strain tensor. ' \
                            'Use symbols "x, y, z, t, this%[parameter_name]": ')
            ezz0 = raw_input('\nEnter expression of the zz component of the infinitesimal ' \
                            'strain tensor. ' \
                            'Use symbols "x, y, z, t, this%[parameter_name]": ')
            exy0 = raw_input('\nEnter expression of the xy component of the infinitesimal ' \
                            'strain tensor. ' \
                            'Use symbols "x, y, z, t, this%[parameter_name]": ')
            exz0 = raw_input('\nEnter expression of the xz component of the infinitesimal ' \
                            'strain tensor. ' \
                            'Use symbols "x, y, z, t, this%[parameter_name]": ')
            eyz0 = raw_input('\nEnter expression of the yz component of the infinitesimal ' \
                            'strain tensor. ' \
                            'Use symbols "x, y, z, t, this%[parameter_name]": ')
            divu0 = raw_input('\nEnter expression of the initial divergence of the solid ' \
                              'displacement. ' \
                              'Use symbols "x, y, z, t, this%[parameter_name]": ')

    approximation_error = raw_input('\nDo you want to compute the absolute (a) or ' \
                                    'relative (r) norm of the approximation error? ')

    ### Choose whether to save simulations or not
    flag = raw_input('\nDo you want to save simulations (y/n)? ')
    if flag == 'y':
        save_simulation = '.True.'
        if problem_type == 1 or problem_type == 2:
            nsteps_to_be_skipped = '0'
        else:
            nsteps_to_be_skipped = raw_input('\nEnter number of time steps to skip each time ' \
                                         'step is saved: ')
    else:
        save_simulation = '.False.'
        nsteps_to_be_skipped = '0'


    ###########################################################################################
    # Initialize fields of method object

    deg = raw_input('\nDegree of the local polynomial spaces (>= 1): ')
    eoa = raw_input('\nExtra order of accuracy for quadrature formulas: ')
    p_local_space = raw_input('\nOrder of local space for fluid pressure (0: P^k, 1: P^{k+1}): ')

    tauS = raw_input('\nType of stabilization function for the solid phase:' \
                     '\n* "n": No stabilization.\n* "d": Diffusion.\n\nChoice: ')
    if tauS == 'd':
        tauS_method = raw_input('\nOrder of tauS (choices: -1, 0, 1): ')
        tauS_constant = raw_input('\nMultiplicative constant for tauS: ')
        bartauS_method = tauS_method
        bartauS_constant = tauS_constant
    else:
        # Anything would be fine here, because it won't be used anyway
        tauS_method = '10'
        tauS_constant = '0.0d+0'
        bartauS_method = tauS_method
        bartauS_constant = tauS_constant

    tauF = raw_input('\nType of stabilization function for the fluid phase:' \
                     '\n* "n": No stabilization.\n* "d": Diffusion.\n\nChoice: ')
    if tauF == 'd':
        tauF_method = raw_input('\nOrder of tauF (choices: -1, 0, 1): ')
        tauF_constant = raw_input('\nMultiplicative constant for tauF: ')
        bartauF_method = raw_input('\nOrder of bartauF (choices: -1, 0, 1): ')
        bartauF_constant = raw_input('\nMultiplicative constant for bartauF: ')
    else:
        # Anything would be fine here, because it won't be used anyway
        tauF_method = '10'
        tauF_constant = '0.0d+0'
        bartauF_method = tauF_method
        bartauF_constant = tauF_constant

    flag_equil_local = raw_input('\nDo you want to equilibrate the linear systems associated to ' \
                                 'the local solvers (y/n)? ')
    if flag_equil_local == 'y':
        equil_local = '.True.'
    else:
        equil_local = '.False.'

    gfm_update_strategy_k = raw_input('\nChoose a stabilization strategy for the ' \
                                      'permeability in the global flux matrix ' \
                                      '(0, no stab; 1, constant stab; 2, SG stab): ')
    if gfm_update_strategy_k == '0' or gfm_update_strategy_k == '2':
        gfm_eps = '0.0d+0'
    else:
        gfm_eps = raw_input('\nConstant stabilization value: ')

    flag_havg_k_gfm  = raw_input('\nDo you want to replace the permeability coefficient ' \
                                 'with its harmonic mean in the global flux matrix (y/n)? ')
    if flag_havg_k_gfm == 'y':
        havg_k_gfm = '.True.'
    else:
        havg_k_gfm = '.False.'

    flag_do_prec = raw_input('\nDo you want to use a preconditioner (y/n)? ')
    if flag_do_prec == 'y':
        do_prec = '.True.'
        prec_update_strategy_k = raw_input('\nChoose a stabilization strategy for the ' \
                                          'permeability in the preconditioning matrix ' \
                                          '(0, no stab; 1, constant stab; 2, SG stab): ')
        if prec_update_strategy_k == '0' or prec_update_strategy_k == '2':
            prec_eps = '0.0d+0'
        else:
            prec_eps = raw_input('\nConstant stabilization value: ')
        flag_havg_k_prec = raw_input('\nDo you want to replace the permeability coefficient ' \
                                     'with its harmonic mean in the preconditioner (y/n)? ')
        if flag_havg_k_prec == 'y':
            havg_k_prec = '.True.'
        else:
            havg_k_prec = '.False.'
    else:
        do_prec                = '.False.'
        prec_update_strategy_k = '0'
        prec_eps               = '0.0d+0'
        havg_k_prec            = '.False.'

    if problem_type == 2 or problem_type == 4:
        maxit_picard = raw_input('\nMaximum number of fixed point iterations: ')
        tolerance    = raw_input('\nType of tolerance to be checked: ' \
                                 '"a", absolute; "r", relative. Choice: ')
        if tolerance == 'a':
            abstol = raw_input('\nAbsolute tolerance: ')
            reltol = '0.0d+0'
        else:
            abstol = '0.0d+0'
            reltol = raw_input('\nRelative tolerance: ')

        flag_rre = raw_input('\nDo you want to use Reduced Rank Extrapolation (y/n)? ')
        if flag_rre == 'y':
            do_rre = '.True.'
            rre_rank = raw_input('\nRank of the generalized inverse ' \
                                 ' (related to the number of iterates to keep): ')
            to_be_discarded = raw_input('\nNumber of initial iterates to discard: ')
        else:
            do_rre = '.False.'
            rre_rank = '0'
            to_be_discarded = '0'
    else:
        maxit_picard = 1
        tolerance = 'x'
        abstol = '0.0d+0'
        reltol = '0.0d+0'
        do_rre = '.False.'
        rre_rank = '0'
        to_be_discarded = '0'

    ### Choose what to export

    exporter = raw_input('\nChoose a type of exporter (P0/P1): ')

    flag = raw_input('\nDo you want to compute and export von Mises stresses for ' \
                     'the effective solid stress tensor (y/n)? ')
    if flag == 'y':
        export_vonMises = '.True.'
    else:
        export_vonMises = '.False.'

    flag = raw_input('\nDo you want to compute and export average maximum ' \
                     'and minimum principal strains (y/n)? ')
    if flag == 'y':
        export_strains = '.True.'
    else:
        export_strains = '.False.'

    flag = raw_input('\nDo you want to compute and export average porosity (y/n)? ')
    if flag == 'y':
        export_poro = '.True.'
    else:
        export_poro = '.False.'

    flag = raw_input('\nDo you want to compute and export elastic energy (y/n)? ')
    if flag == 'y':
        export_Ee = '.True.'
    else:
        export_Ee = '.False.'

    flag = raw_input('\nDo you want to compute and export rate of change of fluid ' \
                     'kinetic energy (y/n)? ')
    if flag == 'y':
        export_W = '.True.'
    else:
        export_W = '.False.'

    #############################################################################################
    # Update the module source code with all the user defined inputs

    module_src = module_src.format(basefilename, \
                                   destdir, \
                                   meshfiles_basename, \
                                   coupled_permeability, \
                                   min_poro, max_poro, initial_poro, mu_fl, \
                                   x_char, t_char, u_char, p_char, s_char, v_char, \
                                   dim_Nf, dim_Ns, \
                                   dim_DirNeu_xy, dim_DirNeu_xz, dim_DirNeu_yz, \
                                   dim_DirNeu_x, dim_DirNeu_y, dim_DirNeu_z, \
                                   physical_flags, \
                                   Tstart, Tend, nsteps, \
                                   approximation_error, save_simulation, \
                                   nsteps_to_be_skipped, \
                                   overload_set_fluid_source_term, \
                                   exporter, export_vonMises, export_strains, \
                                   export_poro, export_Ee, export_W, \
                                   coupled_k, \
                                   uncoupled_k, \
                                   source_fluid, Ff, \
                                   source_solid_x, source_solid_y, source_solid_z, \
                                   mu_e, mu_v, lambda_e, lambda_v, delta, \
                                   gDf, \
                                   gDs1, gDs2, gDs3, \
                                   gD_DirNeu_xy_1, gD_DirNeu_xy_2, \
                                   gD_DirNeu_xz_1, gD_DirNeu_xz_2, \
                                   gD_DirNeu_yz_1, gD_DirNeu_yz_2, \
                                   gD_DirNeu_x, gD_DirNeu_y, gD_DirNeu_z, \
                                   gNf1, gNf2, gNf3, \
                                   gNs1, gNs2, gNs3, gNs4, gNs5, gNs6, \
                                   gN_DirNeu_xy_1, gN_DirNeu_xy_2, gN_DirNeu_xy_3, \
                                   gN_DirNeu_xz_1, gN_DirNeu_xz_2, gN_DirNeu_xz_3, \
                                   gN_DirNeu_yz_1, gN_DirNeu_yz_2, gN_DirNeu_yz_3, \
                                   gN_DirNeu_x_1, gN_DirNeu_x_2, gN_DirNeu_x_3, \
                                   gN_DirNeu_x_4, gN_DirNeu_x_5, \
                                   gN_DirNeu_y_1, gN_DirNeu_y_2, gN_DirNeu_y_3, \
                                   gN_DirNeu_y_4, gN_DirNeu_y_5, \
                                   gN_DirNeu_z_1, gN_DirNeu_z_2, gN_DirNeu_z_3, \
                                   gN_DirNeu_z_4, gN_DirNeu_z_5, \
                                   ux0, uy0, uz0, exx0, eyy0, ezz0, exy0, exz0, eyz0, divu0, \
                                   Txx, Tyy, Tzz, Txy, Txz, Tyz, \
                                   ux, uy, uz, \
                                   vx, vy, vz, \
                                   p, \
                                   deg, eoa, p_local_space, \
                                   tauS, tauF, \
                                   tauS_method, tauF_method, \
                                   bartauS_method, bartauF_method, \
                                   tauS_constant, tauF_constant, \
                                   bartauS_constant, bartauF_constant, \
                                   equil_local, \
                                   gfm_update_strategy_k, gfm_eps, havg_k_gfm, \
                                   do_prec, prec_update_strategy_k, prec_eps, havg_k_prec, \
                                   maxit_picard, tolerance, abstol, reltol, \
                                   do_rre, rre_rank, to_be_discarded)

    f.write(module_src)
    f.close()

    # Move testcase module to ../cases
    newfilename = os.path.join(os.pardir,'cases',filename)
    shutil.copy2(filename, newfilename)
    os.remove(filename)

    todo = '\nNow, before compiling your program, you have to:\n' \
           '1. Edit {}:\n' \
           '   a. In the body of testcase_class, add all the parameters used to define:\n' \
           '      the coupled inverse permeability, the uncoupled inverse permeability, and\n' \
           '      the viscoelastic coefficients.\n' \
           '   b. If this is not a convergence study, you also have to add any parameter used\n' \
           '      to define source terms, boundary and initial conditions.\n' \
           '   c. If this is a convergence study and constant have been specified in single\n' \
           '      precision format for the purpose of automatic differentiation, then you have\n' \
           '      to rewrite them in double precision format.\n' \
           '   d. If this is a time dependent problem and you used the initial/final time\n' \
           '      Tstart/Tend in the definition of your data, replace Tstart/Tend with\n' \
           '      this%Tstart/this%Tend.'.format(newfilename)
    print todo

if __name__ == '__main__':

    try:
        inputfile = sys.argv[1];

        f = open(inputfile, 'r')
        lines = f.readlines()
        f.close()
        parsed_lines = \
            [line.strip().split()[0]+'\n' for line in lines if not line.strip().startswith('#')]

        f = open('tmp.txt', 'w')
        f.writelines(parsed_lines)
        f.close()

        # The module is basically calling itself, but it will enter the 'except' clause
        subprocess.check_call("python tools.py < tmp.txt", shell=True)
        os.remove('tmp.txt')

    except:
        problem_type = int(raw_input('Script for helping writing a module with problem specific data that will be loaded by the HDG library to run a test case. Please enter the type of problem you want to solve:\n(1) stationary, linear.\n(2) stationary, nonlinear.\n(3) time dependent, linear.\n(4) time dependent, nonlinear.\n\nChoice: '))
        write_module(problem_type)


