from pylab import *

def p0t(t, n):
    p = zeros(len(t))
    for k in range(1,n+1):
        p = p + (-1.0)**k*exp(-k**2*pi**2*t)
    p = -6.0*p
    return p

def uLo2(t, n):
    u = 0.5 + zeros(len(t))
    for k in range(1,n+1):
        u = u + 2.0/(k*pi)*(-1.0)**k*sin(k*pi*0.5)*exp(-k**2*pi**2*t)
    return u

t = linspace(0,1,1000)
nlist = [10,15,20,25,50,100]

figure()
for n in nlist:
    p = p0t(t,n)
    plot(t,p)

plot(t,0*t+3.0,'k')
title('Pressure at the origin')
legend(['n = '+str(n) for n in nlist])
show()

figure()
for n in nlist:
    u = uLo2(t,n)
    plot(t,u)

title('ui at L/2')
legend(['n = '+str(n) for n in nlist])
show()

