
program main
  use testcase, only: testcase_problem
  implicit none
  include "silo_f9x.inc"

  ! --------------------- !
  ! Variable declarations !
  ! --------------------- !

  type(testcase_problem)                 :: myproblem
  ! integer, parameter                     :: nexpr = 1
  integer                                :: dbfile, errcode1, errcode2, it
  ! integer                                :: expr_types(nexpr), expr_lnames(nexpr), &
  !                                         & expr_ldefs(nexpr), oldlen
  integer                                :: Nelt, Nver, i, dummy, shapesize(1), shapecounts(1)
  character(len=512)                     :: filename
  character(len=128)                     :: fmt
  ! character(len=65)                      :: expr_names(nexpr), expr_defs(nexpr)

  integer, dimension(:), pointer         :: elements => null()
  real(kind=8), dimension(:,:), pointer  :: coordinates => null()
  real(kind=8), dimension(:), pointer    :: buffer => null(), &
                                          & buffer1 => null(), buffer2 => null(), &
                                          & buffer3 => null(), buffer4 => null(), &
                                          & buffer5 => null(), buffer6 => null()
  
  ! -----------------------------------------------------------------------------------------------

  filename = ""
  fmt      = ""

  print *, ""
  print *, "Script for writing the .silo file(s) associated to a poroviscoelastic problem"
  print *, "--------------------------------------------------------------------------"
  print *, ""

  call myproblem%initialize()

  ! -------------- !
  ! Load mesh data !
  ! -------------- !

  ! Read .ele file

  filename = trim(myproblem%absolute_path_meshfiles_basename) // ".ele"
  open(unit=8, file=trim(filename), status="OLD", iostat=errcode1, action="READ")
  if (errcode1 /= 0) stop "Something went wrong while opening .ele file"

  read(8, *) Nelt

  allocate( elements(4*Nelt), buffer(Nelt), stat=errcode1 )
  if (errcode1 /= 0) stop "Error while allocating elements' arrays"
  
  do i = 1, Nelt
     read(8, *) dummy, elements(4*i-3), elements(4*i-2), elements(4*i-1), elements(4*i)
  end do
  close(unit=8)

  ! Read .node file

  filename = trim(myproblem%absolute_path_meshfiles_basename) // ".node"
  open(unit=8, file=trim(filename), status="OLD", iostat=errcode1, action="READ")
  if (errcode1 /= 0) stop "Something went wrong while opening .node file"

  read(8, *) Nver

  allocate( coordinates(Nver,3), stat=errcode1 )
  if (errcode1 /= 0) stop "Error while allocating coordinates array"

  do i = 1, Nver
     read(8, *) dummy, coordinates(i, :)
  end do
  close(unit=8)

  if (myproblem%exporter == 'P0') then
     allocate( buffer1(Nelt), buffer2(Nelt), buffer3(Nelt), &
       & buffer4(Nelt), buffer5(Nelt), buffer6(Nelt), stat=errcode1 )
     if (errcode1 /= 0) stop "Error while allocating elements' arrays"
  else
     allocate( buffer1(Nver), stat=errcode1 )
     if (errcode1 /= 0) stop "Error while allocating elements' arrays"
  end if

  ! ----------------- !
  ! Create Silo files !
  ! ----------------- !

  ! Shape type has 3 nodes (tri)
  shapesize = (/ 4 /)
  shapecounts = (/ Nelt /)

  write(fmt, *) myproblem%nsteps
  write(fmt, *) len_trim(adjustl(fmt))
  fmt = "i0." // trim(adjustl(fmt))

  do it = 1, myproblem%nsteps
     if (mod(it-1, myproblem%nsteps_to_be_skipped+1) == 0) then
        ! Create Silo file name
        filename = ""
        write(filename, "(A,'/',A," // trim(fmt) // ",'.silo')") &
             & trim(myproblem%destdir), trim(myproblem%myname), it

        ! Create Silo file
        errcode1 = dbcreate(trim(filename), len_trim(filename), DB_CLOBBER, &
             & DB_LOCAL, trim(myproblem%myname) // " simulation results", &
             & len_trim(myproblem%myname) + len(" simulation results"), &
             & DB_HDF5, dbfile)
        if (dbfile == -1) stop "Could not create Silo file!"

        ! Write out connectivity information
        errcode2 = dbputzl(dbfile, "zonelist", 8, Nelt, 3, elements, 4*Nelt, 1, &
             & shapesize, shapecounts, 1, errcode1)

        ! Write an unstructured mesh
        errcode2 = dbputum(dbfile, "mesh", 4, 3, coordinates(1,1), coordinates(1,2), &
             & coordinates(1,3), &
             & "X", 1, "Y", 1, "Z", 1, DB_DOUBLE, Nver, Nelt, "zonelist", 8, DB_F77NULL, &
             & 0, DB_F77NULL, errcode1)

        ! ---------------------------------- !
        ! Write zone-centered only variables !
        ! ---------------------------------- !

        ! von Mises stress
        if (myproblem%export_vonMises) then
           filename = ""
           write(filename, &
                & "(A,'/vMises'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening von Mises file"

           read(8, *) dummy
           do i = 1, Nelt
              read(8, *) buffer(i)
           end do
           close(unit=8)

           errcode2 = dbputuv1(dbfile, "vonMisesStress", 14, "mesh", 4, buffer, &
                & Nelt, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
        end if

        ! Max principal strain
        if (myproblem%export_strains) then
           ! Max strain
           filename = ""
           write(filename, &
                & "(A,'/max_strain'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening max strain file"

           read(8, *) dummy
           do i = 1, Nelt
              read(8, *) buffer(i)
           end do
           close(unit=8)

           errcode2 = dbputuv1(dbfile, "MaxPrincipalStrain", 18, "mesh", 4, buffer, &
                & Nelt, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)

           ! Min strain
           filename = ""
           write(filename, &
                & "(A,'/min_strain'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening min strain file"

           read(8, *) dummy
           do i = 1, Nelt
              read(8, *) buffer(i)
           end do
           close(unit=8)

           errcode2 = dbputuv1(dbfile, "MinPrincipalStrain", 18, "mesh", 4, buffer, &
                & Nelt, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
        end if

        ! Elastic energy
        if (myproblem%export_Ee) then
           filename = ""
           write(filename, &
                & "(A,'/Ee'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening elastic energy file"

           read(8, *) dummy
           do i = 1, Nelt
              read(8, *) buffer(i)
           end do
           close(unit=8)

           errcode2 = dbputuv1(dbfile, "ElasticEnergy", 13, "mesh", 4, buffer, &
                & Nelt, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
        end if
        
        ! Rate of change of blood kinetic energy
        if (myproblem%export_W) then
           filename = ""
           write(filename, &
                & "(A,'/W'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening rate of change of blood k energy file"

           read(8, *) dummy
           do i = 1, Nelt
              read(8, *) buffer(i)
           end do
           close(unit=8)

           errcode2 = dbputuv1(dbfile, "RateChangeFluidKineticEn", 24, "mesh", 4, buffer, &
                & Nelt, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
        end if

        ! ----------------------------------------------------- !
        ! Save data that can be either P0 or P1: Th, uh, vh, ph !
        ! ----------------------------------------------------- !

        if (myproblem%exporter == 'P0') then

           ! Total stress
           filename = ""
           write(filename, "(A,'/Th'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening total stress file"

           read(8, *) dummy
           do i = 1, Nelt
              read(8, *) buffer1(i) ! Txx
              read(8, *) buffer2(i) ! Tyy
              read(8, *) buffer3(i) ! Tzz
              read(8, *) buffer4(i) ! Txy
              read(8, *) buffer5(i) ! Txz
              read(8, *) buffer6(i) ! Tyz
           end do
           close(unit=8)

           errcode2 = dbputuv1(dbfile, "TotalStress_XX", 14, "mesh", 4, buffer1, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
           errcode2 = dbputuv1(dbfile, "TotalStress_YY", 14, "mesh", 4, buffer2, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
           errcode2 = dbputuv1(dbfile, "TotalStress_ZZ", 14, "mesh", 4, buffer3, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
           errcode2 = dbputuv1(dbfile, "TotalStress_XY", 14, "mesh", 4, buffer4, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
           errcode2 = dbputuv1(dbfile, "TotalStress_XZ", 14, "mesh", 4, buffer5, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
           errcode2 = dbputuv1(dbfile, "TotalStress_YZ", 14, "mesh", 4, buffer6, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)

           ! expr_names(1) = ""
           ! expr_names(1) = "TotalStress                                                      "
           ! expr_defs(1) = ""
           ! expr_defs(1) = "{{TotalStress_XX,TotalStress_XY},{0,TotalStress_YY}}             "
           ! expr_lnames = (/11/)! (/11, 12, 13, 10/)
           ! expr_ldefs  = (/65/)! (/65, 31, 33, 24/)
           ! expr_types  = (/DB_VARTYPE_SYMTENSOR/)

           ! oldlen = dbget2dstrlen()
           ! errcode2 = dbset2dstrlen(65)
           ! errcode2 = dbputdefvars(dbfile, "defvars", 7, nexpr, expr_names, expr_lnames, &
           !      & expr_types, expr_defs, expr_ldefs, DB_F77NULL, errcode1)
           ! errcode2 = dbset2dstrlen(oldlen)

           ! Displacement field

           filename = ""
           write(filename, "(A,'/uh'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening displacement file"

           read(8, *) dummy
           do i = 1, Nelt
              read(8, *) buffer1(i) ! ux
              read(8, *) buffer2(i) ! uy
              read(8, *) buffer3(i) ! uz
           end do
           close(unit=8)

           errcode2 = dbputuv1(dbfile, "Displacement_X", 14, "mesh", 4, buffer1, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
           errcode2 = dbputuv1(dbfile, "Displacement_Y", 14, "mesh", 4, buffer2, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
           errcode2 = dbputuv1(dbfile, "Displacement_Z", 14, "mesh", 4, buffer3, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)

           ! expr_names(1) = ""
           ! expr_names(1) = "Displacement                                                     "   
           ! expr_defs(1) = ""
           ! expr_defs(1) = "{Displacement_X,Displacement_Y}                                  "
           ! expr_lnames = (/12/)
           ! expr_ldefs  = (/31/)
           ! expr_types  = (/DB_VARTYPE_VECTOR/)

           ! oldlen = dbget2dstrlen()
           ! errcode2 = dbset2dstrlen(65)
           ! errcode2 = dbputdefvars(dbfile, "defvars", 7, nexpr, expr_names, expr_lnames, &
           !      & expr_types, expr_defs, expr_ldefs, DB_F77NULL, errcode1)
           ! errcode2 = dbset2dstrlen(oldlen)

           ! Darcy velocity

           filename = ""
           write(filename, "(A,'/vh'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening Darcy velocity file"

           read(8, *) dummy
           do i = 1, Nelt
              read(8, *) buffer1(i) ! vx
              read(8, *) buffer2(i) ! vy
              read(8, *) buffer3(i) ! vz
           end do
           close(unit=8)

           errcode2 = dbputuv1(dbfile, "DarcyVelocity_X", 15, "mesh", 4, buffer1, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
           errcode2 = dbputuv1(dbfile, "DarcyVelocity_Y", 15, "mesh", 4, buffer2, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)
           errcode2 = dbputuv1(dbfile, "DarcyVelocity_Z", 15, "mesh", 4, buffer3, Nelt, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)

           ! Fluid pressure

           filename = ""
           write(filename, "(A,'/ph'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening pressure file"

           read(8, *) dummy
           do i = 1, Nelt
              read(8, *) buffer1(i) ! p
           end do
           close(unit=8)

           errcode2 = dbputuv1(dbfile, "FluidPressure", 13, "mesh", 4, buffer1, Nelt, DB_F77NULL, &
                & 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, errcode1)

        else

           ! Total stress
           ! ------------

           filename = ""
           write(filename, "(A,'/Th'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening total stress file"

           read(8, *) dummy

           ! Txx
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "TotalStress_XX", 14, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           ! Tyy
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "TotalStress_YY", 14, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           ! Tzz
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "TotalStress_ZZ", 14, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           ! Txy
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "TotalStress_XY", 14, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           ! Txz
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "TotalStress_XZ", 14, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           ! Tyz
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "TotalStress_YZ", 14, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           close(unit=8)

           ! Solid displacement
           ! ------------------

           filename = ""
           write(filename, "(A,'/uh'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening displacement file"

           read(8, *) dummy

           ! ux
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "Displacement_X", 14, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           ! uy
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "Displacement_Y", 14, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           ! uz
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "Displacement_Z", 14, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           close(unit=8)

           ! Discharge velocity
           ! ------------------

           filename = ""
           write(filename, "(A,'/vh'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening Darcy velocity file"

           read(8, *) dummy

           ! vx
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "DarcyVelocity_X", 15, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           ! vy
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "DarcyVelocity_Y", 15, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           ! vz
           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "DarcyVelocity_Z", 15, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           close(unit=8)

           ! Fluid pressure
           ! --------------

           filename = ""
           write(filename, "(A,'/ph'," // trim(fmt) // ",'.txt')") trim(myproblem%destdir), it
           open(unit=8, file=trim(filename), iostat=errcode1, status="OLD", action="READ")
           if (errcode1 /= 0) stop "Error while opening pressure file"

           read(8, *) dummy

           do i = 1, Nver
              read(8, *) buffer1(i)
           end do
           errcode2 = dbputuv1(dbfile, "FluidPressure", 13, "mesh", 4, buffer1, Nver, &
                & DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, errcode1)

           close(unit=8)

        end if

        ! Close file
        ! ----------
        errcode1 = dbclose(dbfile)
        
     end if
  end do

! -----------------------------

  deallocate(elements, coordinates, buffer)

  if (myproblem%exporter == 'P0') then
     deallocate(buffer1, buffer2, buffer3, buffer4, buffer5, buffer6)
  else
     deallocate(buffer1)
  end if

end program main
