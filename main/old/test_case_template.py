module_string = """
module testcase
  use geometric_structures, only: mesh

  ! Load method_class to create a 'method' object
  ! with testcase-specific method data
  use method_class, only: method

  ! Load problem_class to create a 'problem' object
  ! with testcase-specific sobroutines
  use problem_class, only: problem

  use error_handling, only: check_err1

  implicit none

#include <petsc/finclude/petscsys.h>

  ! ------------------------------------- !
  ! Declare your concrete 'problem' class !
  ! ------------------------------------- !

  type, extends(problem) :: testcase_problem
     
     contains
       procedure, public :: initialize => initialize_problem_
       procedure, public :: set_from_options => set_from_options_

       procedure, public :: permeability_coupled => permeability_coupled_sub
       procedure, public :: permeability_uncoupled => permeability_uncoupled_sub
       procedure, public :: source_fluid => source_fluid_sub
       procedure, public :: set_fluid_source_term => set_fluid_source_term_sub
       procedure, public :: source_solid => source_solid_sub
       procedure, public :: viscoel_coef => viscoel_coef_sub

       procedure, public :: dirichlet_fluid => dirichlet_fluid_sub
       procedure, public :: dirichlet_solid => dirichlet_solid_sub
       procedure, public :: neumann_fluid => neumann_fluid_sub
       procedure, public :: neumann_solid => neumann_solid_sub
       procedure, public :: initial_displacement => initial_displacement_sub
       procedure, public :: initial_strain => initial_strain_sub
       procedure, public :: initial_div_displacement => initial_div_displacement_sub

       procedure, public :: exact_total_stress => exact_total_stress_sub
       procedure, public :: exact_solid_displacement => exact_solid_displacement_sub
       procedure, public :: exact_discharge_velocity => exact_discharge_velocity_sub
       procedure, public :: exact_fluid_pressure => exact_fluid_pressure_sub

  end type testcase_problem

  ! ------------------------------------ !
  ! Declare your specific 'method' class !
  ! ------------------------------------ !

  type, public, extends(method) :: testcase_method
     contains
       procedure, public :: initialize => initialize_method_
  end type testcase_method

  ! --------------- !
  ! Restrict access !
  ! --------------- !

  private :: initialize_problem_, set_from_options_, &
       & permeability_coupled_sub, permeability_uncoupled_sub, &
       & source_fluid_sub, set_fluid_source_term_sub, source_solid_sub, viscoel_coef_sub, &
       & dirichlet_fluid_sub, dirichlet_solid_sub, neumann_fluid_sub, neumann_solid_sub, &
       & initial_displacement_sub, initial_strain_sub, initial_div_displacement_sub, &
       & exact_total_stress_sub, exact_solid_displacement_sub, &
       & exact_discharge_velocity_sub, exact_fluid_pressure_sub

  private :: initialize_method_

contains

  ! --------------------------------------------- !
  ! Implement fields of concrete 'problem' object !
  ! --------------------------------------------- !

  subroutine initialize_problem_(this)
    implicit none

    class(testcase_problem) :: this

    this%myname  = ""
    this%myname  = "{}"

    this%destdir = ""
    this%destdir = "{}"

    this%absolute_path_meshfiles_basename = ""
    this%absolute_path_meshfiles_basename = "{}"

    this%coupled_permeability = {}
    this%min_poro = {}
    this%max_poro = {}
    this%initial_poro = {}
    this%mu_fl = {}

    this%x_char = {}
    this%t_char = {}
    this%u_char = {}
    this%p_char = {}
    this%s_char = {}
    this%v_char = {}

    this%dim_Nf = {}
    this%dim_Ns = {}
    this%dim_DirNeu_xy = {}
    this%dim_DirNeu_xz = {}
    this%dim_DirNeu_yz = {}
    this%dim_DirNeu_x  = {}
    this%dim_DirNeu_y  = {}
    this%dim_DirNeu_z  = {}

    this%physical_flags = {}

    this%Tstart = {}
    this%Tend   = {}
    this%nsteps = {}

    this%approximation_error = "{}"
    this%save_simulation = {}
    this%nsteps_to_be_skipped = {}

    this%overload_set_fluid_source_term = {}
    this%read_projected_initial_displacement = .False.

    this%exporter        = "{}"
    this%export_vonMises = {}
    this%export_strains  = {}
    this%export_poro     = {}
    this%export_Ee       = {}
    this%export_W        = {}

  end subroutine initialize_problem_


  subroutine set_from_options_(this)
    implicit none

    class(testcase_problem) :: this
  end subroutine set_from_options_


  subroutine permeability_coupled_sub(this, phi, k, update_strategy, eps, Th)
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: phi
    PetscReal, dimension(:,:), intent(out) :: k
    integer, intent(in)                    :: update_strategy
    PetscReal, intent(in), optional        :: eps
    type(mesh), optional                   :: Th

    PetscInt                               :: m, n, i
    PetscReal, dimension(:,:), pointer     :: Da => null()
    integer                                :: ierr
    character(len=128)                     :: err_msg

    err_msg = ""

    k = ({})*(this%p_char/(this%v_char*this%x_char))
    select case (update_strategy)
       case (0)
          ! Do nothing
       case (1)
          ! Add a constant value to k on each element

          if (.not. present(eps)) then
             ierr = 1
             call check_err1(ierr, '(A)', 'Input eps absent in permeability_coupled_sub')
          else
             k = k + eps
          end if
       case (2)
          ! Use a stabilization strategy of Scharfetter-Gummel type
          
          if (.not. present(Th)) then
             ierr = 1
             call check_err1(ierr, '(A)', 'Input Th absent in permeability_coupled_sub')
          else
             m = size(phi,1)
             n = size(phi,2)

             allocate( Da(m,n), stat=ierr, errmsg=err_msg )
             call check_err1(ierr, '(A)', trim(err_msg))

             ! Compute the Darcy number
             do i = 1, m
                Da(i,:) = k(i,:)*this%mu_fl/( Th%diameters**2 )
             end do

             ! Scharfetter-Gummel stabilization
             k = k * ( Da + 2.0d+0*Da/(exp(2.0d+0*Da)-1.0d+0) )

             deallocate(Da)
          end if
       case default

          ierr = 1
          call check_err1(ierr, '(A)', 'Unknown stabilization function')

    end select

  end subroutine permeability_coupled_sub


  subroutine permeability_uncoupled_sub(this, x, y, z, t, k)
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: k

    k = ({})*(this%p_char/(this%v_char*this%x_char))

  end subroutine permeability_uncoupled_sub


  subroutine source_fluid_sub(this, x, y, z, t, f, Th)
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: f
    type(mesh)                             :: Th

    f = ({}) * (this%x_char/this%v_char)

  end subroutine source_fluid_sub


  subroutine set_fluid_source_term_sub(this, Th, met, t, Ff)
    implicit none

    class(testcase_problem)                :: this
    type(mesh)                             :: Th
    class(method)                          :: met
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: Ff

    {}

  end subroutine set_fluid_source_term_sub


  subroutine source_solid_sub(this, x, y, z, t, f, Th)
    ! ---------------------------------------------------------
    ! Output:
    !        f   : a 3D array.
    !              * f(:,:,1) = x component of the solid source
    !              * f(:,:,2) = y component of the solid source
    !              * f(:,:,3) = z component of the solid source
    ! ---------------------------------------------------------
    implicit none

    class(testcase_problem)                  :: this
    PetscReal, dimension(:,:), intent(in)    :: x, y, z
    PetscReal, intent(in)                    :: t
    PetscReal, dimension(:,:,:), intent(out) :: f
    type(mesh)                               :: Th

    f(:,:,1) = ({})*(this%x_char/this%s_char)
    f(:,:,2) = ({})*(this%x_char/this%s_char)
    f(:,:,3) = ({})*(this%x_char/this%s_char)

  end subroutine source_solid_sub


  subroutine viscoel_coef_sub(this, x, y, z, t, inv_dt, c1, c2, c3, c4, mu_e_out, lambda_e_out)
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t, inv_dt
    PetscReal, dimension(:,:), intent(out) :: c1, c2, c3, c4
    PetscReal, dimension(:,:), intent(out), optional :: mu_e_out, lambda_e_out

    PetscReal, dimension(:,:), pointer :: mu_e => null(), mu_v => null(), mu_tot => null(), &
         & lambda_e => null(), lambda_v => null(), delta => null()
    PetscInt           :: m, n
    integer            :: ierr
    character(len=128) :: err_msg

    m = size(x,1)
    n = size(x,2)
    err_msg = ""

    allocate( mu_e(m,n), mu_v(m,n), mu_tot(m,n), lambda_e(m,n), lambda_v(m,n), delta(m,n), &
            & stat=ierr, errmsg=err_msg )
    call check_err1(ierr, '(A)', trim(err_msg))

    mu_e = ({}) * (this%u_char/this%x_char)
    mu_v = ({}) * (this%u_char/(this%x_char*this%t_char))
    lambda_e = ({}) * (this%u_char/this%x_char)
    lambda_v = ({}) * (this%u_char/(this%x_char*this%t_char))
    delta = {}

    mu_tot = mu_e + delta * inv_dt * mu_v
    
    c1 = 0.5d+0/mu_tot
    c2 = -0.5d+0/( mu_tot * (2.0d+0 * mu_tot/(lambda_e + delta * inv_dt * lambda_v) + 3.0d+0) )
    c3 = delta * inv_dt * 2.0d+0 * mu_v * c1
    c4 = delta * inv_dt * (2.0d+0 * mu_v * c2 + lambda_v * (3.0d+0*c2 + c1))

    if (present(mu_e_out)) then
       mu_e_out     = mu_e
       lambda_e_out = lambda_e
    end if

    deallocate( mu_e, mu_v, mu_tot, lambda_e, lambda_v, delta )

  end subroutine viscoel_coef_sub

  ! ---------------------------------------------------------------------
  ! Boundary and initial conditions
  ! ---------------------------------------------------------------------

  subroutine dirichlet_fluid_sub(this, x, y, z, t, p, Th)
    ! -----------------------------------------------
    ! Compute the Dirichlet condition on the fluid pressure
    ! -----------------------------------------------
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: p
    type(mesh)                             :: Th

    p = ({}) * (1.0d+0/this%p_char)

  end subroutine dirichlet_fluid_sub


  subroutine dirichlet_solid_sub(this, x, y, z, t, u, marker, Th)
    ! -----------------------------------------------
    ! Compute the Dirichlet condition on the solid displacement
    ! -----------------------------------------------
    implicit none

    class(testcase_problem)                  :: this
    PetscReal, dimension(:,:), intent(in)    :: x, y, z
    PetscReal, intent(in)                    :: t
    PetscReal, dimension(:,:,:), intent(out) :: u
    character(len=1), intent(in)             :: marker
    type(mesh)                               :: Th

    PetscErrorCode                           :: ierr
    character(len=128)                       :: err_msg

    ! -----------------------------------------------------------

    err_msg = ""

    select case (marker)
       case ('1')
          ! Standard Dirichlet on x,y,z
          u(:,:,1) = ({}) * (1.0d+0/this%u_char)
          u(:,:,2) = ({}) * (1.0d+0/this%u_char)
          u(:,:,3) = ({}) * (1.0d+0/this%u_char)

       case ('3')
          ! Dirichlet on x and y
          u(:,:,1) = ({}) * (1.0d+0/this%u_char)
          u(:,:,2) = ({}) * (1.0d+0/this%u_char)

       case ('4')
          ! Dirichlet on x and z
          u(:,:,1) = ({}) * (1.0d+0/this%u_char)
          u(:,:,2) = ({}) * (1.0d+0/this%u_char)

       case ('5')
          ! Dirichlet on y and z
          u(:,:,1) = ({}) * (1.0d+0/this%u_char)
          u(:,:,2) = ({}) * (1.0d+0/this%u_char)

       case ('6')
          ! Dirichlet on x 
          u(:,:,1) = ({}) * (1.0d+0/this%u_char)

       case ('7')
          ! Dirichlet on y
          u(:,:,1) = ({}) * (1.0d+0/this%u_char)

       case ('8')
          ! Dirichlet on z
          u(:,:,1) = ({}) * (1.0d+0/this%u_char)

       case default
          ierr = 1
          call check_err1(ierr, '(A)', 'Unknown type of Dirichlet boundary condition')

    end select
       
  end subroutine dirichlet_solid_sub


  subroutine neumann_fluid_sub(this, x, y, z, t, gNf, Th)
    ! Output:
    !        gNf : ALWAYS a 3D array containing evaluations of Neumann boundary
    !              conditions. If BC are given in a standard way, then size(gNf,3) = 1;
    !              otherwise, size(gNf,3) = 3, where
    !              * gNf(:,:,1) = gNf_x
    !              * gNf(:,:,2) = gNf_y
    !              * gNf(:,:,3) = gNf_z
    implicit none

    class(testcase_problem)                  :: this
    PetscReal, dimension(:,:), intent(in)    :: x, y, z
    PetscReal, intent(in)                    :: t
    PetscReal, dimension(:,:,:), intent(out) :: gNf
    type(mesh)                               :: Th

    {}
    {}
    {}

  end subroutine neumann_fluid_sub


  subroutine neumann_solid_sub(this, x, y, z, t, gNs, marker, Th)
    ! Output:
    !        gNs : a 3D array containing evaluations of Neumann boundary
    !              conditions. They can be given both in standard and 'nonstandard' way,
    !              i.e. by specifying all the required components of the total stress tensor.
    !              We can have up to 7 different types of Neumann BCs:
    !              - marker = '2': full Neumann BCs.
    !                * Standard form: gNs(:,:,1) = gNs_x, gNs(:,:,2) = gNs_y,
    !                           gNs(:,:,3) = gNs_z.
    !                * Non standard form: gNs(:,:,1) = gNs_xx, gNs(:,:,2) = gNs_yy,
    !                           gNs(:,:,3) = gNs_zz; gNs(:,:,4) = gNs_xy; gNs(:,:,5) = gNs_xz;
    !                           gNs(:,:,6) = gNs_yz.
    !              - marker = '3': Neumann on z.
    !                * Standard form: gNs(:,:,1) = gNs_z.
    !                * Non standard form: gNs(:,:,1) = gNs_zz, gNs(:,:,2) = gNs_xz,
    !                           gNs(:,:,3) = gNs_yz.
    !              - marker = '4': Neumann on y.
    !                * Standard form: gNs(:,:,1) = gNs_y.
    !                * Non standard form: gNs(:,:,1) = gNs_yy, gNs(:,:,2) = gNs_xy,
    !                           gNs(:,:,3) = gNs_yz.
    !              - marker = '5': Neumann on x.
    !                * Standard form: gNs(:,:,1) = gNs_x.
    !                * Non standard form: gNs(:,:,1) = gNs_xx, gNs(:,:,2) = gNs_xy,
    !                           gNs(:,:,3) = gNs_xz.
    !              - marker = '6': Neumann on y,z.
    !                * Standard form: gNs(:,:,1) = gNs_y, gNs(:,:,2) = gNs_z.
    !                * Non standard form: gNs(:,:,1) = gNs_yy, gNs(:,:,2) = gNs_zz,
    !                           gNs(:,:,3) = gNs_xy; gNs(:,:,4) = gNs_xz; gNs(:,:,5) = gNs_yz.
    !              - marker = '7': Neumann on x,z.
    !                * Standard form: gNs(:,:,1) = gNs_x, gNs(:,:,2) = gNs_z.
    !                * Non standard form: gNs(:,:,1) = gNs_xx, gNs(:,:,2) = gNs_zz,
    !                           gNs(:,:,3) = gNs_xy; gNs(:,:,4) = gNs_xz; gNs(:,:,5) = gNs_yz.
    !              - marker = '8': Neumann on x,y.
    !                * Standard form: gNs(:,:,1) = gNs_x, gNs(:,:,2) = gNs_y.
    !                * Non standard form: gNs(:,:,1) = gNs_xx, gNs(:,:,2) = gNs_yy,
    !                           gNs(:,:,3) = gNs_xy; gNs(:,:,4) = gNs_xz; gNs(:,:,5) = gNs_yz.
    implicit none

    class(testcase_problem)                  :: this
    PetscReal, dimension(:,:), intent(in)    :: x, y, z
    PetscReal, intent(in)                    :: t
    PetscReal, dimension(:,:,:), intent(out) :: gNs
    character(len=1), intent(in)             :: marker
    type(mesh)                               :: Th

    PetscErrorCode                           :: ierr
    character(len=128)                       :: err_msg

    ! -----------------------------------------------------------

    err_msg = ""

    select case (marker)
       case ('2')
          ! Standard Neumann on x,y,z
          {}
          {}
          {}
          {}
          {}
          {}

       case ('3')
          ! Neumann on z
          {}
          {}
          {}

       case ('4')
          ! Neumann on y
          {}
          {}
          {}

       case ('5')
          ! Neumann on x
          {}
          {}
          {}

       case ('6')
          ! Neumann on y,z
          {}
          {}
          {}
          {}
          {}

       case ('7')
          ! Neumann on x,z
          {}
          {}
          {}
          {}
          {}

       case ('8')
          ! Neumann on x,y
          {}
          {}
          {}
          {}
          {}

       case default
          ierr = 1
          call check_err1(ierr, '(A)', 'Unknown type of Neumann boundary condition')

    end select

  end subroutine neumann_solid_sub


  subroutine initial_displacement_sub(this, x, y, z, ux0, uy0, uz0)
    ! -----------------------------------------------
    ! Compute the initial condition on solid displacement
    ! -----------------------------------------------
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, dimension(:,:), intent(out) :: ux0, uy0, uz0
  
    ux0 = ({}) * (1.0d+0/this%u_char)
    uy0 = ({}) * (1.0d+0/this%u_char)
    uz0 = ({}) * (1.0d+0/this%u_char)

  end subroutine initial_displacement_sub


  subroutine initial_strain_sub(this, x, y, z, exx0, eyy0, ezz0, exy0, exz0, eyz0)
    ! ----------------------------------------------------------------
    ! Compute the initial condition on the infinitesimal strain tensor
    ! ----------------------------------------------------------------
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, dimension(:,:), intent(out) :: exx0, eyy0, ezz0, exy0, exz0, eyz0

    exx0 = ({}) * (this%x_char/this%u_char)
    eyy0 = ({}) * (this%x_char/this%u_char)
    ezz0 = ({}) * (this%x_char/this%u_char)
    exy0 = ({}) * (this%x_char/this%u_char)
    exz0 = ({}) * (this%x_char/this%u_char)
    eyz0 = ({}) * (this%x_char/this%u_char)

  end subroutine initial_strain_sub


  subroutine initial_div_displacement_sub(this, x, y, z, divu)
    ! -----------------------------------------------
    ! Compute the initial divergence of the solid displacement
    ! -----------------------------------------------
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, dimension(:,:), intent(out) :: divu

    divu = ({}) * (this%x_char/this%u_char)

  end subroutine initial_div_displacement_sub
  
  ! ---------------------------------------------------------------------
  ! Exact solution
  ! ---------------------------------------------------------------------
  
  subroutine exact_total_stress_sub(this, x, y, z, t, Txx, Tyy, Tzz, Txy, Txz, Tyz)
    ! -----------------------------------------------
    ! Compute the exact total stress
    ! -----------------------------------------------
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: Txx, Tyy, Tzz, Txy, Txz, Tyz

    Txx = ({}) * (1.0d+0/this%s_char)
    Tyy = ({}) * (1.0d+0/this%s_char)
    Tzz = ({}) * (1.0d+0/this%s_char)
    Txy = ({}) * (1.0d+0/this%s_char)
    Txz = ({}) * (1.0d+0/this%s_char)
    Tyz = ({}) * (1.0d+0/this%s_char)

  end subroutine exact_total_stress_sub


  subroutine exact_solid_displacement_sub(this, x, y, z, t, ux, uy, uz)
    ! -----------------------------------------------
    ! Compute the exact solid displacement
    ! -----------------------------------------------
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: ux, uy, uz

    ux = ({}) * (1.0d+0/this%u_char)
    uy = ({}) * (1.0d+0/this%u_char)
    uz = ({}) * (1.0d+0/this%u_char)

  end subroutine exact_solid_displacement_sub


  subroutine exact_discharge_velocity_sub(this, x, y, z, t, vx, vy, vz)
    ! -----------------------------------------------
    ! Compute the exact discharge velocity
    ! -----------------------------------------------
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: vx, vy, vz

    vx = ({}) * (1.0d+0/this%v_char)
    vy = ({}) * (1.0d+0/this%v_char)
    vz = ({}) * (1.0d+0/this%v_char)

  end subroutine exact_discharge_velocity_sub


  subroutine exact_fluid_pressure_sub(this, x, y, z, t, p)
    ! -----------------------------------------------
    ! Compute the exact fluid pressure
    ! -----------------------------------------------
    implicit none

    class(testcase_problem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: p

    p = ({}) * (1.0d+0/this%p_char)

  end subroutine exact_fluid_pressure_sub

  ! ------------------------------------ !
  ! Initialize fields of 'method' object !
  ! ------------------------------------ !

  subroutine initialize_method_(this)
    implicit none

    class(testcase_method) :: this

    this%k = {}
    this%eoa = {}
    this%p_local_space = {}

    this%tauS = "{}"
    this%tauF = "{}"

    this%tauS_method = {}
    this%tauF_method = {}

    this%bartauS_method = {}
    this%bartauF_method = {}

    this%tauS_constant = {}
    this%tauF_constant = {}

    this%bartauS_constant = {}
    this%bartauF_constant = {}

    this%equil_local = {}

    this%gfm_update_strategy_k = {}
    this%gfm_eps               = {}
    this%havg_k_gfm            = {}

    this%do_prec                = {}
    this%prec_update_strategy_k = {}
    this%prec_eps               = {}
    this%havg_k_prec            = {}

    this%maxit_picard = {}
    this%tolerance    = "{}"
    this%abstol       = {}
    this%reltol       = {}

    this%do_rre          = {}
    this%rre_rank        = {}
    this%to_be_discarded = {}

  end subroutine initialize_method_


end module testcase

"""
