=========================
hdg3d_poroviscoelasticity
=========================

**hdg3d_poroviscoelasticity** is a Fortran library developed in `PETSc <https://petsc.org/release/>`_ for solving Partial Differential Equations (PDEs) with Hybridizable Discontinuous Galerkin Methods.

|

License
=======

MIT License

Copyright (c) 2023, Daniele Prada

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

|

Compiling
=========

Prerequisites:

* `VTKFortran <https://github.com/szaghi/VTKFortran>`_

Step by step procedure:

1. update the variable ``VTKFORTRAN_DIR`` in ``<HDG3D_ROOT/library/makefile>``
2. ``cd <HDG3D_ROOT/library>`` and then ``make libhdg3d``
3. update absolute paths in ``<HDG3D_ROOT/meshes/MESHnames.inc>``
4. update the variable ``HDG3D_DIR`` in ``<HDG3D_ROOT/cases/makefile>``
5. ``cd <HDG3D_ROOT/cases>`` and then ``make libcases``

|

Main programs
=============

A few example programs are available in the folder ``<HDG3d_ROOT/main>``:

* ``convtest``: convergence analysis of standalone Biot solver;
* ``convtest_ode``: convergence analysis of ODE solvers;
* ``pqp_algo``, ``qpq_algo``, ``os_algo``: PQP/QPQ/OS algorithms for PDE-ODE coupling via integral interface conditions;
* ``pqp_robin_algo, os_robin_algo``: PQP/OS algorithms for PDE-ODE coupling via Robin-like interface conditions.

Input data for these example programs are specified by some environment variables defined in ``<HDG3d_ROOT/main/makefile>``. After properly tuning these variables, to compile and run any of the example programs (e.g., ``os_algo``) just type::

  make run-os_algo

|

Publications
============

* L.Bociu, G.Guidoboni, D.Prada, and R.Sacco. `Numerical simulation and analysis of multiscale interface coupling between a poroelastic medium and a lumped hydraulic circuit: comparison between functional iteration and operator splitting methods <https://www.sciencedirect.com/science/article/pii/S0021999122004417>`_. Journal of Computational Physics 466, 2022.
* D.Prada. `A Hybridizable Discontinuous Galerkin Method for Nonlinear Porous Media Viscoelasticity with Applications in Ophthalmology <https://drive.google.com/open?id=1yazgXBItym1DA6qYOyzDOMXvgQqJ_Hvj>`_. Indiana University-Purdue University Indianapolis, December 2016.

|

Contact Us
==========

Please, `contact us`_ if you need any assistance.

.. _contact us: prada@imati.cnr.it
