# Mesh for JCP testcase
PRISM_MESH_SHORT = prism
PRISM_MESH_ROOT  = /home/daniele/Documents/codes/hdg3d_poroviscoelasticity/meshes
PRISM_MESH_BASE_NAMES = \
	prism_biot.2

UNITCUBE_MESH_SHORT = cube
UNITCUBE_MESH_ROOT  = /home/daniele/Documents/codes/hdg3d_poroviscoelasticity/meshes
UNITCUBE_MESH_BASE_NAMES = \
	unitcube.2
