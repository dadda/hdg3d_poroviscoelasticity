! Testing hdg2d_basis_structure module

program main
  use hdg3d_basis_structure, only: basis3d, construct_basis, destruct_basis, quadrature_hdg
  implicit none

#include <petsc/finclude/petscsys.h>

  type(basis3d)      :: mybasis
  PetscErrorCode     :: errorcode
  PetscInt           :: k, eoa, i
  PetscMPIInt        :: rank

  PetscInt :: n
  PetscInt, parameter :: npts = 6
  PetscReal, dimension(npts) :: quaderror, quaderror1, quaderror2
  PetscReal, dimension(:,:), pointer :: formula

  ! ----------------------------------

  call PetscInitialize(PETSC_NULL_CHARACTER, errorcode)

  call mpi_comm_rank(PETSC_COMM_WORLD, rank, errorcode)

  ! --------------------
  ! Testing construct_basis
  ! --------------------

  ! if (rank == 0) then
  !    print *, "Enter the order of the local spaces:"
  !    read(*,*) k
  !    print *, "Enter extra order of accuracy for quadrature formulas:"
  !    read(*,*) eoa
  ! endif

  k = 3
  eoa = 5

  call construct_basis(mybasis, k, eoa)
  if (rank == 0) then
     print *, ""
     print *, "2D quadrature formula:"
     do i = 1, ubound(mybasis%formula2d, 1)
        print "(3(es22.15, 1X))", mybasis%formula2d(i,1:3)
     end do
     print *, "Check weight sum:", sum(mybasis%formula2d(:,4))

     print *, ""
     print *, "3D quadrature formula:"
     do i = 1, ubound(mybasis%formula3d, 1)
        print "(4(es22.15, 1X))", mybasis%formula3d(i,1:4)
     end do
     print *, "Check weight sum:", sum(mybasis%formula3d(:,5))
  endif

  call destruct_basis(mybasis)

  ! ------------------------------------------------------
  ! Check 2D quadrature rule
  !
  ! Compute the integral of sin(x-y)*sin(x+y) on the reference
  ! triangle.
  ! ------------------------------------------------------

  if (rank == 0) then
     print *, ""
     print *, "Checking 2D quadrature rule..."
  endif
  do i = 1, npts
     n = 10*(2**(i-1))
     call quadrature_hdg(2*n+1, 2, formula)

     ! The exact value of this integral is 0.
     quaderror(i) = abs( sum( formula(:,4)*fun2(formula(:,2), formula(:,3)) ) )

     deallocate(formula)
  end do
  if (rank == 0) print *, "Errors: ", quaderror

  ! ------------------------------------------------------
  ! Check 3D quadrature rule
  !
  ! Compute several integrals on the reference tetrahedron.
  ! - a) f1(x,y,z) = z. \int f1 = 1/24
  ! - b) f2(x,y,z) = x*exp(-y). \int f2 = exp(-1) - 1/3
  ! - c) f3(x,y,z) = cos(x + y + z). \int f3 = cos(1) - sin(1)/2
  ! ------------------------------------------------------

  if (rank == 0) then
     print *, ""
     print *, "Checking 3D quadrature rule..."
  endif
  do i = 1, npts
     n = 10*(2**(i-1))
     call quadrature_hdg(2*n+1, 3, formula)

     ! \int f1
     quaderror(i)  = abs( 1.0d+0/24.0d+0 - sum( formula(:,5)*formula(:,4) )/6.0d+0 )
     ! \int f2
     quaderror1(i) = abs( exp(-1.0d+0) - 1.0d+0/3.0d+0 - &
          & sum( formula(:,5)*f2( formula(:,2), formula(:,3), formula(:,4)) )/6.0d+0 )
     ! \int f3
     quaderror2(i) = abs( cos(1.0d+0) - sin(1.0d+0)/2.0d+0 - &
          & sum( formula(:,5)*f3( formula(:,2), formula(:,3), formula(:,4)) )/6.0d+0 )

     deallocate(formula)
  end do
  if (rank == 0) then
     print *, "Errors for \int f1: ", quaderror
     print *, "Errors for \int f2: ", quaderror1
     print *, "Errors for \int f3: ", quaderror2
  end if

  ! -----------------------------------------------------------------

  call PetscFinalize(errorcode)

contains

  ! elemental PetscReal function fun1(x, alpha)
  !   implicit none

  !   PetscReal, intent(in) :: x
  !   PetscInt, intent(in) :: alpha

  !   fun1 = abs(x)**(alpha + 3.d0/5.d0)
  ! end function fun1

  elemental PetscReal function fun2(x, y)
    implicit none

    PetscReal, intent(in) :: x, y

    fun2 = sin(x-y)*sin(x+y)
  end function fun2

  elemental PetscReal function f2(x, y, z)
    implicit none

    PetscReal, intent(in) :: x, y, z

    f2 = x*exp(-y)
  end function f2

  elemental PetscReal function f3(x, y, z)
    implicit none

    PetscReal, intent(in) :: x, y, z

    f3 = cos(x + y + z)
  end function f3

end program main
