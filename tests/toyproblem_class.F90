
module toyproblem_class
  use geometric_structures, only: mesh

  ! Load problem_class to create a 'problem' object
  ! with toyproblem_class-specific sobroutines
  use problem_class, only: problem

  ! Load method_class to create a 'method' object
  ! with toyproblem_class-specific method data
  use method_class, only: method

  use error_handling, only: check_err1

  implicit none

#include <petsc/finclude/petscsys.h>

  ! ------------------------------------- !
  ! Declare your concrete 'problem' class !
  ! ------------------------------------- !

  type, extends(problem) :: toyproblem
     
     contains
       procedure, public :: initialize => initialize_problem_

       procedure, public :: permeability_coupled => permeability_coupled_sub
       procedure, public :: permeability_uncoupled => permeability_uncoupled_sub
       procedure, public :: source_fluid => source_fluid_sub
       procedure, public :: source_solid => source_solid_sub
       procedure, public :: viscoel_coef => viscoel_coef_sub

       procedure, public :: dirichlet_fluid => dirichlet_fluid_sub
       procedure, public :: dirichlet_solid => dirichlet_solid_sub
       procedure, public :: neumann_fluid => neumann_fluid_sub
       procedure, public :: neumann_solid => neumann_solid_sub
       procedure, public :: initial_displacement => initial_displacement_sub
       procedure, public :: initial_div_displacement => initial_div_displacement_sub

       procedure, public :: exact_total_stress => exact_total_stress_sub
       procedure, public :: exact_solid_displacement => exact_solid_displacement_sub
       procedure, public :: exact_discharge_velocity => exact_discharge_velocity_sub
       procedure, public :: exact_fluid_pressure => exact_fluid_pressure_sub

  end type toyproblem

  ! ------------------------------------ !
  ! Declare your specific 'method' class !
  ! ------------------------------------ !

  type, public, extends(method) :: toymethod
     contains
       procedure, public :: initialize => initialize_method_
  end type toymethod

  ! --------------- !
  ! Restrict access !
  ! --------------- !

  private :: initialize_problem_, permeability_coupled_sub, permeability_uncoupled_sub, &
       & source_fluid_sub, source_solid_sub, viscoel_coef_sub, &
       & dirichlet_fluid_sub, dirichlet_solid_sub, neumann_fluid_sub, neumann_solid_sub, &
       & initial_displacement_sub, initial_div_displacement_sub, &
       & exact_total_stress_sub, exact_solid_displacement_sub, &
       & exact_discharge_velocity_sub, exact_fluid_pressure_sub

  private :: initialize_method_

contains

  ! --------------------------------------------- !
  ! Implement fields of concrete 'problem' object !
  ! --------------------------------------------- !

  subroutine initialize_problem_(this)
    implicit none

    class(toyproblem) :: this

    this%myname  = ""
    this%myname  = "test_coupled_stationary_poly"

    this%destdir = ""
    this%destdir = "/home/daniele/Documents/codes/hdg3d_poroviscoelasticity/results/tcsp"

    this%absolute_path_meshfiles_basename = ""
    this%absolute_path_meshfiles_basename = "/home/daniele/Documents/codes/hdg3d_poroviscoelasticity/meshes/unitcube_nelt6.1"

    this%coupled_permeability = .True.
    this%min_poro = 0.1d+0
    this%max_poro = 0.9d+0
    this%initial_poro = 0.5d+0
    this%mu_fl = 1.0d+0

    this%dim_Nf = 3
    this%dim_Ns = 6

    this%Tend = -1.0d+0
    this%nsteps = 1
    this%nsteps_to_be_skipped = 0

  end subroutine initialize_problem_


  subroutine permeability_coupled_sub(this, phi, k, update_strategy, eps, Th)
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: phi
    PetscReal, dimension(:,:), intent(out) :: k
    integer, intent(in)                    :: update_strategy
    PetscReal, intent(in), optional        :: eps
    type(mesh), optional                   :: Th

    PetscInt                               :: m, n, i
    PetscReal, dimension(:,:), pointer     :: Da => null()
    integer                                :: ierr
    character(len=128)                     :: err_msg

    err_msg = ""

    k = phi**2
    select case (update_strategy)
       case (0)
          ! Do nothing
       case (1)
          ! Add a constant value to k on each element

          if (.not. present(eps)) then
             ierr = 1
             call check_err1(ierr, '(A)', 'Input eps absent in permeability_coupled_sub')
          else
             k = k + eps
          end if
       case (2)
          ! Use a stabilization strategy of Scharfetter-Gummel type
          
          if (.not. present(Th)) then
             ierr = 1
             call check_err1(ierr, '(A)', 'Input Th absent in permeability_coupled_sub')
          else
             m = size(phi,1)
             n = size(phi,2)

             allocate( Da(m,n), stat=ierr, errmsg=err_msg )
             call check_err1(ierr, '(A)', trim(err_msg))

             ! Compute the Darcy number
             do i = 1, m
                Da(i,:) = k(i,:)*this%mu_fl/( Th%diameters**2 )
             end do

             ! Scharfetter-Gummel stabilization
             k = k * ( Da + 2.0d+0*Da/(exp(2.0d+0*Da)-1.0d+0) )

             deallocate(Da)
          end if
       case default

          ierr = 1
          call check_err1(ierr, '(A)', 'Unknown stabilization function')

    end select

  end subroutine permeability_coupled_sub


  subroutine permeability_uncoupled_sub(this, x, y, z, t, k)
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: k

    k = 0.0d+0

  end subroutine permeability_uncoupled_sub


  subroutine source_fluid_sub(this, x, y, z, t, f)
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: f

    f = -x*(0.2*x**2 + 0.1*y*z + 0.5)*(0.2*y**3 + 0.8*y**2*z + 0.4*y*z**2 + 2*z*(0.2*x**2 + 0.1*y*z + 0.5))

  end subroutine source_fluid_sub


  subroutine source_solid_sub(this, x, y, z, t, f)
    ! ---------------------------------------------------------
    ! Output:
    !        f   : a 3D array.
    !              * f(:,:,1) = x component of the solid source
    !              * f(:,:,2) = y component of the solid source
    !              * f(:,:,3) = z component of the solid source
    ! ---------------------------------------------------------
    implicit none

    class(toyproblem)                  :: this
    PetscReal, dimension(:,:), intent(in)    :: x, y, z
    PetscReal, intent(in)                    :: t
    PetscReal, dimension(:,:,:), intent(out) :: f

    f(:,:,1) = 0.8*x - y**2*z
    f(:,:,2) = -2*x*y*z + 0.2*y + 0.2*z
    f(:,:,3) = -x*y**2 + 0.2*y + 0.2*z

  end subroutine source_solid_sub


  subroutine viscoel_coef_sub(this, x, y, z, t, inv_dt, c1, c2, c3, c4)
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t, inv_dt
    PetscReal, dimension(:,:), intent(out) :: c1, c2, c3, c4

    PetscReal, dimension(:,:), pointer :: mu_e => null(), mu_v => null(), mu_tot => null(), &
         & lambda_e => null(), lambda_v => null(), delta => null()
    PetscInt           :: m, n
    integer            :: ierr
    character(len=128) :: err_msg

    m = size(x,1)
    n = size(x,2)
    err_msg = ""

    allocate( mu_e(m,n), mu_v(m,n), mu_tot(m,n), lambda_e(m,n), lambda_v(m,n), delta(m,n), &
            & stat=ierr, errmsg=err_msg )
    call check_err1(ierr, '(A)', trim(err_msg))

    mu_e = 1
    mu_v = 0
    lambda_e = 1
    lambda_v = 0
    delta = 0

    mu_tot = mu_e + delta * inv_dt * mu_v
    
    c1 = 0.5d+0/mu_tot
    c2 = -0.5d+0/( mu_tot * (2.0d+0 * mu_tot/(lambda_e + delta * inv_dt * lambda_v) + 3.0d+0) )
    c3 = delta * inv_dt * 2.0d+0 * mu_v * c1
    c4 = delta * inv_dt * (2.0d+0 * mu_v * c2 + lambda_v * (3.0d+0*c2 + c1))

    deallocate( mu_e, mu_v, mu_tot, lambda_e, lambda_v, delta )

  end subroutine viscoel_coef_sub

  ! ---------------------------------------------------------------------
  ! Boundary and initial conditions
  ! ---------------------------------------------------------------------

  subroutine dirichlet_fluid_sub(this, x, y, z, t, p)
    ! -----------------------------------------------
    ! Compute the Dirichlet condition on the fluid pressure
    ! -----------------------------------------------
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: p

    p = x*y**2*z

  end subroutine dirichlet_fluid_sub


  subroutine dirichlet_solid_sub(this, x, y, z, t, ux, uy, uz)
    ! -----------------------------------------------
    ! Compute the Dirichlet condition on the solid displacement
    ! -----------------------------------------------
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: ux, uy, uz

    ux = 0.1*x*y*z
    uy = 0.1*x**2*y
    uz = 0.1*x**2*z
       
  end subroutine dirichlet_solid_sub


  subroutine neumann_fluid_sub(this, x, y, z, t, gNf)
    ! Output:
    !        gNf : ALWAYS a 3D array containing evaluations of Neumann boundary
    !              conditions. If BC are given in a standard way, then size(gNf,3) = 1;
    !              otherwise, size(gNf,3) = 3, where
    !              * gNf(:,:,1) = gNf_x
    !              * gNf(:,:,2) = gNf_y
    !              * gNf(:,:,3) = gNf_z
    implicit none

    class(toyproblem)                  :: this
    PetscReal, dimension(:,:), intent(in)    :: x, y, z
    PetscReal, intent(in)                    :: t
    PetscReal, dimension(:,:,:), intent(out) :: gNf

    gNf(:,:,1) = -y**2*z*(0.2*x**2 + 0.1*y*z + 0.5)**2
    gNf(:,:,2) = -2*x*y*z*(0.2*x**2 + 0.1*y*z + 0.5)**2
    gNf(:,:,3) = -x*y**2*(0.2*x**2 + 0.1*y*z + 0.5)**2

  end subroutine neumann_fluid_sub


  subroutine neumann_solid_sub(this, x, y, z, t, gNs)
    ! Output:
    !        gNs : a 3D array containing evaluations of Neumann boundary
    !              conditions. If BC are given in a standard way, then size(gNf,3) = 3 and
    !              gNs(:,:,1) = gNs_x, gNs(:,:,2) = gNs_y, gNs(:,:,3) = gNs_z;
    !              otherwise, size(gNf,3) = 6, and:
    !              * gNs(:,:,1) = gNs_xx, gNs(:,:,2) = gNs_yy, gNs(:,:,3) = gNs_zz;
    !              * gNs(:,:,4) = gNs_xy, gNs(:,:,5) = gNs_xz, gNs(:,:,6) = gNs_yz.
    implicit none

    class(toyproblem)                  :: this
    PetscReal, dimension(:,:), intent(in)    :: x, y, z
    PetscReal, intent(in)                    :: t
    PetscReal, dimension(:,:,:), intent(out) :: gNs

    gNs(:,:,1) = 0.2*x**2 - x*y**2*z + 0.3*y*z
    gNs(:,:,2) = 0.4*x**2 - x*y**2*z + 0.1*y*z
    gNs(:,:,3) = 0.4*x**2 - x*y**2*z + 0.1*y*z
    gNs(:,:,4) = x*(0.2*y + 0.1*z)
    gNs(:,:,5) = x*(0.1*y + 0.2*z)
    gNs(:,:,6) = 0

  end subroutine neumann_solid_sub


  subroutine initial_displacement_sub(this, x, y, z, ux0, uy0, uz0)
    ! -----------------------------------------------
    ! Compute the initial condition on solid displacement
    ! -----------------------------------------------
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, dimension(:,:), intent(out) :: ux0, uy0, uz0
  
    ux0 = 0.0d+0
    uy0 = 0.0d+0
    uz0 = 0.0d+0

  end subroutine initial_displacement_sub


  subroutine initial_div_displacement_sub(this, x, y, z, divu)
    ! -----------------------------------------------
    ! Compute the initial divergence of the solid displacement
    ! -----------------------------------------------
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, dimension(:,:), intent(out) :: divu
  
    divu = 0.0d+0

  end subroutine initial_div_displacement_sub
  
  ! ---------------------------------------------------------------------
  ! Exact solution
  ! ---------------------------------------------------------------------
  
  subroutine exact_total_stress_sub(this, x, y, z, t, Txx, Tyy, Tzz, Txy, Txz, Tyz)
    ! -----------------------------------------------
    ! Compute the exact total stress
    ! -----------------------------------------------
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: Txx, Tyy, Tzz, Txy, Txz, Tyz

    Txx = 0.2*x**2 - x*y**2*z + 0.3*y*z
    Tyy = 0.4*x**2 - x*y**2*z + 0.1*y*z
    Tzz = 0.4*x**2 - x*y**2*z + 0.1*y*z
    Txy = x*(0.2*y + 0.1*z)
    Txz = x*(0.1*y + 0.2*z)
    Tyz = 0

  end subroutine exact_total_stress_sub


  subroutine exact_solid_displacement_sub(this, x, y, z, t, ux, uy, uz)
    ! -----------------------------------------------
    ! Compute the exact solid displacement
    ! -----------------------------------------------
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: ux, uy, uz

    ux = 0.1*x*y*z
    uy = 0.1*x**2*y
    uz = 0.1*x**2*z

  end subroutine exact_solid_displacement_sub


  subroutine exact_discharge_velocity_sub(this, x, y, z, t, vx, vy, vz)
    ! -----------------------------------------------
    ! Compute the exact discharge velocity
    ! -----------------------------------------------
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: vx, vy, vz

    vx = -y**2*z*(0.2*x**2 + 0.1*y*z + 0.5)**2
    vy = -2*x*y*z*(0.2*x**2 + 0.1*y*z + 0.5)**2
    vz = -x*y**2*(0.2*x**2 + 0.1*y*z + 0.5)**2

  end subroutine exact_discharge_velocity_sub


  subroutine exact_fluid_pressure_sub(this, x, y, z, t, p)
    ! -----------------------------------------------
    ! Compute the exact fluid pressure
    ! -----------------------------------------------
    implicit none

    class(toyproblem)                :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: p

    p = x*y**2*z

  end subroutine exact_fluid_pressure_sub

  ! ------------------------------------ !
  ! Initialize fields of 'method' object !
  ! ------------------------------------ !

  subroutine initialize_method_(this)
    implicit none

    class(toymethod) :: this

    this%k = 1
    this%eoa = 5

    this%tauS = "d"
    this%tauF = "d"

    this%tauS_method = -1
    this%tauF_method = -1

    this%bartauS_method = -1
    this%bartauF_method = -1

    this%tauS_constant = 1.0d+0
    this%tauF_constant = 1.0d+0

    this%bartauS_constant = 1.0d+0
    this%bartauF_constant = 1.0d+0

    this%equil_local = .True.

    this%gfm_update_strategy_k = 0
    this%gfm_eps               = 0.0d+0

    this%do_prec                = .False.
    this%prec_update_strategy_k = 0
    this%prec_eps               = 0.0d+0

    this%maxit_picard = 1000
    this%tolerance    = "r"
    this%abstol       = 0.0d+0
    this%reltol       = 1.0d-12

    this%do_rre          = .False.
    this%rre_rank        = 0
    this%to_be_discarded = 0

  end subroutine initialize_method_


end module toyproblem_class

