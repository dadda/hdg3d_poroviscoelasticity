syms x y z t

mu     = 1;
lambda = 1;
kappa  = eye(3);

p = t^2*x^2;

u = [
  t^2*(3*y+z*x);
  x*y + z^2 + t^2*y;
  z*y + y^2 + t
];

gradu = [
    diff(u(1),x) diff(u(1),y) diff(u(1),z);
    diff(u(2),x) diff(u(2),y) diff(u(2),z);
    diff(u(3),x) diff(u(3),y) diff(u(3),z);
];

strains = (gradu + permute(gradu,[2 1]))/2;

divu = strains(1,1) + strains(2,2) + strains(3,3);

T = 2*mu*strains + lambda * divu * eye(3) - p*eye(3);

v          = -kappa * [diff(p,x); diff(p,y); diff(p,z)];
kappa_sqrt = sqrtm(kappa);
v_scaled   = kappa_sqrt\v;

S = diff(divu,t) + diff(v(1),x) + diff(v(2),y) + diff(v(3),z);
f = -[
    diff(T(1,1),x) + diff(T(1,2),y) + diff(T(1,3),z);
    diff(T(2,1),x) + diff(T(2,2),y) + diff(T(2,3),z);
    diff(T(3,1),x) + diff(T(3,2),y) + diff(T(3,3),z);
];
