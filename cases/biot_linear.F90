
module biot_linear
#include <petsc/finclude/petscsys.h>
  use petscsys
  use physics
  use geometric_structures, only: mesh
  use error_handling,       only: check_err1

  implicit none

contains
  
  subroutine sqrtm_permeability_linear(this, x, y, z, t, &
       & sqrtmK_xx, sqrtmK_yy, sqrtmK_zz, sqrtmK_xy, sqrtmK_xz, sqrtmK_yz)
    implicit none

    class(Biot_data)                     :: this
    PetscReal, dimension(:), intent(in)  :: x, y, z
    PetscReal, intent(in)                :: t
    PetscReal, dimension(:), intent(out) :: sqrtmK_xx, sqrtmK_yy, sqrtmK_zz, &
         & sqrtmK_xy, sqrtmK_xz, sqrtmK_yz

    sqrtmK_xx = 1.0d+0; sqrtmK_yy = 1.0d+0; sqrtmK_zz = 1.0d+0
    sqrtmK_xy = 0.0d+0; sqrtmK_xz = 0.0d+0; sqrtmK_yz = 0.0d+0
  end subroutine sqrtm_permeability_linear


  subroutine diff_sqrtm_permeability_linear(this, x, y, z, t, &
       & dx_sqrtmK_xx, dx_sqrtmK_xy, dx_sqrtmK_xz, &
       & dy_sqrtmK_xy, dy_sqrtmK_yy, dy_sqrtmK_yz, &
       & dz_sqrtmK_xz, dz_sqrtmK_yz, dz_sqrtmK_zz)
    implicit none

    class(Biot_data)                     :: this
    PetscReal, dimension(:), intent(in)  :: x, y, z
    PetscReal, intent(in)                :: t
    PetscReal, dimension(:), intent(out) :: dx_sqrtmK_xx, dx_sqrtmK_xy, dx_sqrtmK_xz, &
         & dy_sqrtmK_xy, dy_sqrtmK_yy, dy_sqrtmK_yz, &
         & dz_sqrtmK_xz, dz_sqrtmK_yz, dz_sqrtmK_zz

    dx_sqrtmK_xx = 0.0d+0; dx_sqrtmK_xy = 0.0d+0; dx_sqrtmK_xz = 0.0d+0
    dy_sqrtmK_xy = 0.0d+0; dy_sqrtmK_yy = 0.0d+0; dy_sqrtmK_yz = 0.0d+0
    dz_sqrtmK_xz = 0.0d+0; dz_sqrtmK_yz = 0.0d+0; dz_sqrtmK_zz = 0.0d+0
  end subroutine diff_sqrtm_permeability_linear


  subroutine source_fluid_linear(this, x, y, z, t, f)
    implicit none

    class(Biot_data)                     :: this
    PetscReal, dimension(:), intent(in)  :: x, y, z
    PetscReal, intent(in)                :: t
    PetscReal, dimension(:), intent(out) :: f

    f = 0.0d+0
  end subroutine source_fluid_linear


  subroutine source_solid_linear(this, x, y, z, t, f)
    implicit none

    class(Biot_data)                       :: this
    PetscReal, dimension(:), intent(in)    :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: f

    f(:,1) = 1.0d+0; f(:,2) = 1.0d+0; f(:,3) = 0.0d+0
  end subroutine source_solid_linear


  subroutine exact_total_stress_linear(this, x, y, z, t, Txx, Tyy, Tzz, Txy, Txz, Tyz)
    implicit none

    class(Biot_data)                       :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: Txx, Tyy, Tzz, Txy, Txz, Tyz

    Txx = 1.0d+0 - x - y
    Tyy = 3.0d+0 - x - y
    Tzz = 1.0d+0 - x - y
    Txy = 3.0d+0*t + 1.0d+0
    Txz = 1.0d+0
    Tyz = 1.0d+0
  end subroutine exact_total_stress_linear


  subroutine exact_solid_displacement_linear(this, x, y, z, t, ux, uy, uz)
    implicit none

    class(Biot_data)                       :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: ux, uy, uz

    ux = t * 3.0d+0 * y
    uy = x + y + z
    uz = x + t
  end subroutine exact_solid_displacement_linear


  subroutine exact_discharge_velocity_linear(this, x, y, z, t, vx, vy, vz)
    implicit none

    class(Biot_data)                       :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: vx, vy, vz

    vx = -1.0d+0; vy = -1.0d+0; vz = 0.0d+0
  end subroutine exact_discharge_velocity_linear


  subroutine exact_fluid_pressure_linear(this, x, y, z, t, p)
    implicit none

    class(Biot_data)                       :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, intent(in)                  :: t
    PetscReal, dimension(:,:), intent(out) :: p

    p = x + y
  end subroutine exact_fluid_pressure_linear


  subroutine dirichlet_fluid_linear(this, x, y, z, t, bm, Th, p)
    ! Especially useful for the PQP method
    implicit none

    class(Biot_data)                                :: this
    PetscReal, dimension(:,:), intent(in)           :: x, y, z
    PetscReal, intent(in)                           :: t
    class(BiotBM)                                   :: bm
    type(mesh)                                      :: Th
    PetscReal, dimension(:,:), pointer, intent(out) :: p

    allocate(p(size(x,1),size(x,2)))
    p = x + y
  end subroutine dirichlet_fluid_linear


  subroutine neumann_fluid_linear(this, x, y, z, t, bm, Th, gNf)
    implicit none

    class(Biot_data)                                  :: this
    PetscReal, dimension(:,:), intent(in)             :: x, y, z
    PetscReal, intent(in)                             :: t
    class(BiotBM)                                     :: bm
    type(mesh)                                        :: Th
    PetscReal, dimension(:,:,:), pointer, intent(out) :: gNf

    allocate( gNf(size(x,1),size(x,2),3) )
    gNf(:,:,1) = -1.0d+0
    gNf(:,:,2) = -1.0d+0
    gNf(:,:,3) = 0.0d+0
  end subroutine neumann_fluid_linear


  subroutine ibc_fluid_linear(this, t, Th, flags, values_rhs, values)
    implicit none

    class(Biot_data)                              :: this
    PetscReal, intent(in)                         :: t
    type(mesh)                                    :: Th
    PetscInt, dimension(:), pointer, intent(out)  :: flags
    PetscReal, dimension(:), pointer, intent(out) :: values_rhs
    PetscReal, dimension(:), pointer, intent(out) :: values

    allocate( flags(1), values_rhs(1), values(1) )
    flags      = (/ 2 /)
    values_rhs = (/ 1.0d99 /) !(/ -1.0d+0 /)
    values     = (/ 0.0d+0 /)
  end subroutine ibc_fluid_linear


  subroutine robin_fluid_linear(this, x, y, z, t, bm, Th, marker, coeffs, gRf)
    ! Compute the Robin condition on the fluid pressure
    !
    ! The condition is assumed to be given as
    !
    ! coeffs(1) * p + coeffs(2) * \int p + v \dot n = gRf
    !
    ! where coeffs are constant coefficients.
    class(Biot_data)                                :: this
    PetscReal, dimension(:,:), intent(in)           :: x, y, z
    PetscReal, intent(in)                           :: t
    class(BiotBM)                                   :: bm
    type(mesh)                                      :: Th
    character(len=*), intent(in)                    :: marker
    PetscReal, dimension(:,:), pointer, intent(out) :: coeffs
    PetscReal, dimension(:,:), pointer, intent(out) :: gRf

    select case (marker)
    case ('standard')
       allocate( coeffs(1,size(x,2)), gRf(size(x,1),size(x,2)) )
       gRf = 0.1d+0 * (x + y) - 1.0d+0
       coeffs = 0.1d+0
    case ('integral')
       allocate( coeffs(2,size(x,2)), gRf(size(x,1),size(x,2)) )
       gRf = 0.1d+0 * (x + y) - 0.5d+0
       coeffs(1,:) = 0.1d+0
       coeffs(2,:) = 1.0d+0/3.0d+0
    end select
  end subroutine robin_fluid_linear


  subroutine pressure_ibc_linear(this, t, Th, flags, values_rhs)
    class(Biot_data)                              :: this
    PetscReal, intent(in)                         :: t
    type(mesh)                                    :: Th
    PetscInt, dimension(:), pointer, intent(out)  :: flags
    PetscReal, dimension(:), pointer, intent(out) :: values_rhs

    allocate( flags(1), values_rhs(1) )
    flags      = (/ 2 /)
    values_rhs = (/ 1.5d+0 /)
  end subroutine pressure_ibc_linear


  subroutine dirichlet_solid_linear(this, x, y, z, t, bm, Th, marker, u)
    implicit none

    class(Biot_data)                                  :: this
    PetscReal, dimension(:,:), intent(in)             :: x, y, z
    PetscReal, intent(in)                             :: t
    class(BiotBM)                                     :: bm
    type(mesh)                                        :: Th
    character(len=*), intent(in)                      :: marker
    PetscReal, dimension(:,:,:), pointer, intent(out) :: u
    character(len=128)                                :: err_msg
    PetscErrorCode                                    :: ierr

    err_msg = ''
    select case (marker)
    case ('xyz')
       allocate( u(size(x,1),size(x,2),3), stat=ierr, errmsg=err_msg )
       u(:,:,1) = t * 3.0d+0 * y
       u(:,:,2) = x + y + z
       u(:,:,3) = x + t
       call check_err1(ierr, '(A)', trim(err_msg))
    end select
  end subroutine dirichlet_solid_linear


  subroutine neumann_solid_linear(this, x, y, z, t, bm, Th, marker, gNs)
    implicit none

    class(Biot_data) :: this
    PetscReal, dimension(:,:), intent(in)             :: x, y, z
    PetscReal, intent(in)                             :: t
    class(BiotBM)                                     :: bm
    type(mesh)                                        :: Th
    character(len=*), intent(in)                      :: marker
    PetscReal, dimension(:,:,:), pointer, intent(out) :: gNs
    character(len=128)                                :: err_msg
    PetscErrorCode                                    :: ierr

    err_msg = ''
    select case (marker)
    case ('xyz')
       allocate( gNs(size(x,1), size(x,2), 6), stat=ierr, errmsg=err_msg )
       call check_err1(ierr, '(A)', trim(err_msg))
       gNs(:,:,1) = 1.0d+0 - x - y
       gNs(:,:,2) = 3.0d+0 - x - y
       gNs(:,:,3) = 1.0d+0 - x - y
       gNs(:,:,4) = 3.0d+0*t + 1.0d0+0
       gNs(:,:,5) = 1.0d+0
       gNs(:,:,6) = 1.0d+0
    end select
  end subroutine neumann_solid_linear


  subroutine initial_displacement_linear(this, x, y, z, ux0, uy0, uz0)
    implicit none

    class(Biot_data)                       :: this
    PetscReal, dimension(:,:), intent(in)  :: x, y, z
    PetscReal, dimension(:,:), intent(out) :: ux0, uy0, uz0

    ux0 = 0.0d+0
    uy0 = x + y + z
    uz0 = x
  end subroutine initial_displacement_linear


  subroutine bc_flags_linear(this, flags)
    implicit none

    class(Biot_data)                      :: this
    PetscInt, dimension(:), intent(inout) :: flags
  end subroutine bc_flags_linear

end module biot_linear
