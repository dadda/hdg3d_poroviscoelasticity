function dP = jcp_dP(t, data)
    
    dP = jcp_w0(t,data) - data.c/(data.k*data.a*data.b) * jcp_dQ(t,data);
    
    %if t > 0
        
        for n = 1:data.Nterms
            fun = @(tau) jcp_wn(tau,n,data) .* ...
                exp(-(n^2*pi^2/data.c^2) * ...
                data.k * data.K * (t - tau));
            
            dP = dP + (1-2*mod(n,2)) * ...
                ( -n^2*pi^2/data.c^2*data.k*data.K * ...
                  integral(fun,0,t,'AbsTol',data.atol,'RelTol',data.rtol) ...
                  + jcp_wn(t,n,data) );
                  
              % (2*data.Qtilde*data.alpha*data.c^3*(exp(t*data.alpha) - exp(-n^2*pi^2/data.c^2*data.k*data.K*t)))/(pi^2*data.a*data.b*data.k*n^2*(data.K*data.k*pi^2*n^2 + data.alpha*data.c^2)) ...
        end
    %end
end
