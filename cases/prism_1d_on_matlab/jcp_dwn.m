function dwn = jcp_dwn(t,n,data)
    
    dwn = 2 * (1-2*mod(n,2)) / ...
        (n^2*pi^2) * ...
        data.c / (data.k*data.a*data.b) * ...
        jcp_d2Q(t,data);
    
end
