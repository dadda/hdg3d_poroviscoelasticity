function Pi1 = jcp_Pi1(t,data)
    
    Pi1 = jcp_Pi(t,data) - data.R1 * ...
        jcp_Q1(t,data) - data.L1 * ...
        jcp_dQ1(t,data);
    
end
