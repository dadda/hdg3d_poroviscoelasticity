data = create_data();

N = 1000;
t = linspace(0, data.Tend, N+1);

yex = zeros(N+1,8);
for n = 2:N+1
    if any(n == [164,654,890,935,968])
        yex(n,1) = jcp_Q(t(n),data);
        yex(n,2) = jcp_dQ(t(n),data);
        yex(n,3) = jcp_P(t(n),data);
        yex(n,4) = jcp_dP(t(n),data);
        yex(n,5) = jcp_d2Q(t(n),data);
        yex(n,6) = jcp_d2P(t(n),data);
        yex(n,7) = jcp_d3Q(t(n),data);
        yex(n,8) = jcp_d3P(t(n),data);
    else
        yex(n,:) = [ ...
            jcp_Q(t(n),data) ...
            jcp_dQ(t(n),data) ...
            jcp_P(t(n),data) ...
            jcp_dP(t(n),data) ...
            jcp_d2Q(t(n),data) ...
            jcp_d2P(t(n),data) ...
            jcp_d3Q(t(n),data) ...
            jcp_d3P(t(n),data)
        ];
    end
end
