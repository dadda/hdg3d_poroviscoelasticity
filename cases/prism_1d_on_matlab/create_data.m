function data = create_data()
    
    data.Qtilde = 1e-4;
    data.Tend   = 10;
    data.alpha  = 2/data.Tend; % -log(4)/data.Tend;
    data.s      = 4;
    data.R      = 1;
    data.a      = 0.1;
    data.b      = 0.1;
    data.c      = 0.5;
    data.k      = 1;
    data.K      = 1;
    data.C      = 1e-3;
    data.R1     = 1;
    data.C1     = 1e-1;
    data.L1     = 1;
    data.Rbar   = 1;
    data.Nterms = 30;
    data.atol   = 1e-17;
    data.rtol   = 0;

end
