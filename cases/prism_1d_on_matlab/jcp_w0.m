function w0 = jcp_w0(t, data)
    
    w0 = 2*data.c/(3*data.k*data.a*data.b) * jcp_dQ(t,data) ...
        - data.K/(data.a*data.b*data.c) * jcp_Q(t,data);
    
end
