function dQ1 = jcp_dQ1(t,data)
    
    dQ1 = jcp_dQ(t,data) - data.C * ...
        jcp_d2Pi(t,data);
    
end
