function Pi = jcp_Pi(t, data)
    
    Pi = jcp_P(t,data) - ...
        data.R*jcp_Q(t,data);
    
end
