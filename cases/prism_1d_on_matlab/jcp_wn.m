function wn = jcp_wn(t, n, data)
    
    wn = 2 * (1-2*mod(n,2)) / ...
        (n^2*pi^2) * data.c / (data.k*data.a*data.b) * ...
        jcp_dQ(t,data);
    
end
