syms t T Qtilde alpha a b c k K n tau Pi

Q = Qtilde * exp(alpha*t);
dQ = diff(Q,t);
d2Q = diff(dQ,t);
d3Q = diff(d2Q,t);


w0 = 2*c/(3*k*a*b) * dQ ...
        - K/(a*b*c) * Q;

wn = 2 * (-1)^n / ...
        (n^2*Pi^2) * c / (k*a*b) * dQ;

tmp = exp(n^2*Pi^2/c^2*k*K*t);

int_w0 = int(w0,t);
int_w0 = int_w0 - subs(int_w0,t,0);
int_w0 = simplify(int_w0);

int_wn = int(wn*tmp,t);
int_wn = int_wn - subs(int_wn,t,0);
int_wn = int_wn * exp(-n^2*Pi^2/c^2*k*K*t);
int_wn = simplify(int_wn);


