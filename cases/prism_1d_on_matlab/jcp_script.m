data = create_data();

my_circuit = @(t,y) jcp_circuit(t,y,data);

tspan = [0,10];

y0 = [jcp_Pi(0,data);
      jcp_Pi1(0,data);
      jcp_Q1(0,data)];

% Runge-Kutta
[trk,yrk] = ...
    ode45(my_circuit, tspan, y0);
dt_rk = min(diff(trk));

% Backward Euler
N = ceil(diff(tspan)/dt_rk);

[tbe,ybe] = ...
    beuler(my_circuit, tspan, N, y0,data);

% Exact solution
L = length(tbe);
yex = zeros(L,3);
for n = 1:L
    yex(n,:) = [ ...
        jcp_Pi(tbe(n),data) ...
        jcp_Pi1(tbe(n),data) ...
        jcp_Q1(tbe(n),data) ];
end

% Compare solutions
subplot(3,1,1)
plot(trk,yrk(:,1),'b+-',...
    tbe,ybe(:,1),'r*-',...
    tbe,yex(:,1),'gx-')
title('\pi')
legend('rk','be','exact')

subplot(3,1,2)
plot(trk,yrk(:,2),'b+-',...
    tbe,ybe(:,2),'r*-',...
    tbe,yex(:,2),'gx-')
title('\pi_1')

subplot(3,1,3)
plot(trk,yrk(:,3),'b+-',...
    tbe,ybe(:,3),'r*-',...
    tbe,yex(:,3),'gx-')
title('Q_1')

% Convergence study
nruns = 3;
errors = zeros(nruns,3);
errors(1,:) = max(abs(ybe-yex),[],1);

for i = 2:nruns
N = 2*N;
[tbe,ybe] = ...
    beuler(my_circuit, tspan, N, y0, data);

% Exact solution
L = length(tbe);
yex = zeros(L,3);
for n = 1:L
    yex(n,:) = [ ...
        jcp_Pi(tbe(n),data) ...
        jcp_Pi1(tbe(n),data) ...
        jcp_Q1(tbe(n),data) ];
end
errors(i,:) = max(abs(ybe-yex),[],1);
end


