function Q = jcp_Q(t, data)
    Q = data.Qtilde * ...
        (exp(-(data.alpha*t).^data.s) - 1);
    %Q = data.Qtilde*exp(data.alpha*t);
end
