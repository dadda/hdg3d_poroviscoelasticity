function d2Q1 = jcp_d2Q1(t,data)
   
    d2Q1 = jcp_d2Q(t,data) - data.C * ...
        jcp_d3Pi(t,data);
    
end
