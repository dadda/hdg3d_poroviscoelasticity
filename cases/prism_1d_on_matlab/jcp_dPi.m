function dPi = jcp_dPi(t, data)
    
    dPi = jcp_dP(t,data) - data.R * ...
        jcp_dQ(t,data);
    
end
