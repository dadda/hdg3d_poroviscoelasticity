function d3Pi = jcp_d3Pi(t,data)
    
    d3Pi = jcp_d3P(t,data) - data.R * ...
        jcp_d3Q(t,data);
    
end
