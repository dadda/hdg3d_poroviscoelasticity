function d2Pi = jcp_d2Pi(t,data)
    
    d2Pi = jcp_d2P(t,data) - data.R * ...
        jcp_d2Q(t,data);
    
end