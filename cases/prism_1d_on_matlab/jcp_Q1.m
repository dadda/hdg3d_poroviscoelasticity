function Q1 = jcp_Q1(t,data)
    
    Q1 = jcp_Q(t,data) - ...
        data.C*jcp_dPi(t,data);
    
end
