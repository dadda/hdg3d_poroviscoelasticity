function [t,y] = ...
        beuler(my_ode, tspan, N, y0, data)

t0 = tspan(1);
T = tspan(2);

dt = (T-t0)/N;

t = linspace(t0, T, N+1); t = t';

d = size(y0,1);

y = zeros(N+1,d);

y(1,:) = y0';

u = y0;
% options = optimoptions('fsolve','Display','off');

A = [0 0 -1/data.C;
     0 -1/(data.Rbar*data.C1) 1/data.C1;
     1/data.L1 -1/data.L1 -data.R1/data.L1];
%eig(A)

%U = [data.C 0 0;
%    0 data.C1 0;
%    0 0 data.L1];

%T = -U*A
%eig(T)
%eig(T+T')
    
B = eye(3) - dt*A;

for n = 1:N
    u_old = u;
    
    %fun = @(x) x - u_old - ...
        %dt*my_ode(t(n+1),x);

    %u = fsolve(fun,u_old,options);
    s = [0;
         jcp_pbar(t(n+1),data)/(data.Rbar*data.C1);
         0];
     
    b = [jcp_Q(t(n+1),data)/data.C; 0; 0];

    rhs = u_old+dt*(s+b);

    u = B\rhs;
    y(n+1,:) = u';
end

end