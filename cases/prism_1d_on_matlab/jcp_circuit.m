function dydt = jcp_circuit(t,y,data)
    
    A = [0 0 -1/data.C;
         0 -1/(data.Rbar*data.C1) 1/data.C1;
         1/data.L1 -1/data.L1 -data.R1/data.L1];
     
    s = [0;
         jcp_pbar(t,data)/(data.Rbar*data.C1);
         0];
     
    b = [jcp_Q(t,data)/data.C; 0; 0];
    
    dydt = A*y + s + b;
    
end
