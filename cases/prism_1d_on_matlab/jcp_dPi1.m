function dPi1 = jcp_dPi1(t,data)
    
    dPi1 = jcp_dPi(t,data) - data.R1 * ...
        jcp_dQ1(t,data) - data.L1 * ...
        jcp_d2Q1(t,data);
    
end
