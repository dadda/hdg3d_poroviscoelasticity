import sys
import numpy as np

try:
    inputfile = sys.argv[1]
except:
    print 'Usage: python create_errors_table.py <inputfile with errors from Fortran>'

f = open(inputfile, 'r')
lines = f.readlines()
f.close()

# Read errors first
for i, line in enumerate(lines):
    if line.strip() == 'Errors':
        break

# Find the number of meshes
for j, l in enumerate(lines[i+3].split()):
    if l == ':':
        break

nmeshes = len(lines[i+3].split())  - j - 1

errors = np.zeros([8,nmeshes])
for k in range(8):
    errors[k,:] = np.array([float(m.strip(',')) for m in lines[i+3+k].split()[-nmeshes:]])

print errors

# Read orders of convergence
for i, line in enumerate(lines):
    if line.strip() == 'Orders of convergence':
        break

orders = np.zeros([8,nmeshes-1])
for k in range(8):
    orders[k,:] = np.array([float(m.strip(',')) for m in lines[i+3+k].split()[-nmeshes+1:]])

print orders

# Create table

table_template_part1 = r"""
\begin{table}
\centering
\begin{tabular}{cccccccc}
\toprule
$\eT$ & e.c.r. & $\eu$ & e.c.r. & $\ev$ & e.c.r. & $\ep$ & e.c.r.\\
\midrule
"""

table_template_part2 = r"""
\midrule
$\eubar$ & e.c.r. & $\epbar$ & e.c.r. & $\euhat$ & e.c.r. & $\ephat$ & e.c.r.\\
\midrule
"""

table_template_part3 = r"""
\bottomrule
\end{tabular}
\caption{Update me.}
\label{tab:updateme}
\end{table}
"""

finaltable = table_template_part1

for i in range(nmeshes):
    if i == 0:
        finaltable = finaltable + r'$%10.4e$ & - & $%10.4e$ & - & $%10.4e$ & - & $%10.4e$ & -\\' % tuple(errors[:4,i]) + '\n'
    else:
        dummy = np.c_[errors[:4,i], orders[:4,i-1]].flatten()
        finaltable = finaltable + r'$%10.4e$ & $%3.2f$ & $%10.4e$ & $%3.2f$ & $%10.4e$ & $%3.2f$ & $%10.4e$ & $%3.2f$\\' % tuple(dummy) + '\n'

finaltable = finaltable + table_template_part2

for i in range(nmeshes):
    if i == 0:
        finaltable = finaltable + r'$%10.4e$ & - & $%10.4e$ & - & $%10.4e$ & - & $%10.4e$ & -\\' % tuple(errors[4:,i]) + '\n'
    else:
        dummy = np.c_[errors[4:,i], orders[4:,i-1]].flatten()
        finaltable = finaltable + r'$%10.4e$ & $%3.2f$ & $%10.4e$ & $%3.2f$ & $%10.4e$ & $%3.2f$ & $%10.4e$ & $%3.2f$\\' % tuple(dummy) + '\n'

finaltable = finaltable + table_template_part3

print finaltable

